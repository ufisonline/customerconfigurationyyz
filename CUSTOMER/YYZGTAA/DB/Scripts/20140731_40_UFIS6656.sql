alter table irmtab rename to fshtab;
update systab set tana = 'FSH' where tana = 'IRM';
alter table FSRTAB rename column UIRM TO UFSH;
update systab set fina = 'UFSH' where tana = 'FSR' and FINA = 'UIRM';
comment on column FSRTAB.UFSH is 'Unique Number of FSHTAB';
update systab set addi = 'Unique Number of FSHTAB' where tana = 'FSR' and FINA ='UFSH';
alter table FSHTAB add RTYP	VARCHAR2(20) default	' '	;
comment on column FSHTAB.RTYP is 'Reference Info Type';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FSHTAB' 
and a.column_name = 'RTYP';
commit;

alter table afttab add FLUT char(8) ;
alter table afttab modify flut default ' ';
comment on column afttab.flut is 'Flight Date in UTC';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'AFTTAB' 
and a.column_name = 'FLUT';
commit;