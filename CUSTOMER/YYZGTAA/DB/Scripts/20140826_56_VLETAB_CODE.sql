ALTER TABLE VLETAB ADD CODE CHAR(8);
ALTER TABLE VLETAB MODIFY CODE DEFAULT ' ';
UPDATE VLETAB SET CODE=' ' WHERE CODE IS NULL;
COMMIT;

COMMENT ON COLUMN VLETAB.CODE IS 'Short code for legend definition';
COMMIT;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') ADDI
,a.column_name FINA
,'YYZ' HOPO
,a.data_length FELE
, substr(a.data_type,0,1) FITY
, 'N' INDX 
,'TAB' PROJ
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' SYST
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'VLETAB' 
and a.column_name IN ( 
'CODE'
);

COMMIT;