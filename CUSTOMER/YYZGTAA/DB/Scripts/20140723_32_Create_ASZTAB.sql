CREATE TABLE ASZTAB(
ASIZ  VARCHAR2(5) DEFAULT ' ',
WTFR VARCHAR2(10) DEFAULT ' ',
WTTO VARCHAR2(10) DEFAULT ' ',
URNO NUMBER(10) DEFAULT 0
);


comment on column asztab.asiz is 'Aircraft Size';
comment on column ASZTAB.WTFR IS 'Weight From';
COMMENT ON COLUMN ASZTAB.WTTO IS 'WEIGHT TO';
commit;