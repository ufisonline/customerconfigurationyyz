--select * from user_tab_columns where table_name = 'FSHTAB' and column_name IN ('STAT','TOTC','PROC','ERRC','SUCC','PRBE','PREN');

ALTER TABLE FSHTAB ADD (
TOTC NUMBER default 0,
PROC NUMBER default 0,
ERRC NUMBER default 0,
SUCC NUMBER default 0,
PRBE CHAR(14) default ' ',
PREN CHAR(14) default ' '
);


COMMENT ON COLUMN FSHTAB.TOTC IS 'Number of records in the file';
COMMENT ON COLUMN  FSHTAB.PROC IS 'Number of records that have been';
COMMENT ON COLUMN  FSHTAB.ERRC IS 'Number of records that have been';
COMMENT ON COLUMN  FSHTAB.SUCC IS 'Number of records that have been';
COMMENT ON COLUMN  FSHTAB.PRBE IS 'Process Begin Date/Time';
COMMENT ON COLUMN  FSHTAB.PREN IS 'Process End Date/Time';

DELETE SYSTAB WHERE TANA = 'FSH' AND FINA IN ('TOTC','PROC','ERRC','SUCC','PRBE','PREN');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FSHTAB' 
and a.column_name IN ('TOTC','PROC','ERRC','SUCC','PRBE','PREN');

commit;
