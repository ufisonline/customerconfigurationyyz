ALTER TABLE AFTTAB ADD (
ELDN CHAR(14) ,
ALDN CHAR(14) ,
EIBN CHAR(14) ,
AIBN CHAR(14) ,
EOBN CHAR(14) ,
AOBN CHAR(14) ,
ETON CHAR(14) ,
ATON CHAR(14) 
);

ALTER TABLE AFTTAB MODIFY ELDN DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY ALDN DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY EIBN DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY AIBN DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY EOBN DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY AOBN DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY ETON DEFAULT ' ';
ALTER TABLE AFTTAB MODIFY ATON DEFAULT ' ';

COMMENT ON COLUMN AFTTAB.ELDN IS 'Estimated Landing Time (Airline)';
COMMENT ON COLUMN AFTTAB.ALDN IS 'Actual Landing Time (Airline)';
COMMENT ON COLUMN AFTTAB.EIBN IS 'Estimated In-block Time (Airline)';
COMMENT ON COLUMN AFTTAB.AIBN IS 'Actual In-block Time (Airline)';
COMMENT ON COLUMN AFTTAB.EOBN IS 'Estimated Off-block Time (Airline)';
COMMENT ON COLUMN AFTTAB.AOBN IS 'Actual Off-block Time (Airline)';
COMMENT ON COLUMN AFTTAB.ETON IS 'Estimated Take Off Time (Airline)';
COMMENT ON COLUMN AFTTAB.ATON IS 'Actual Take Off Time (Airline)';

DELETE SYSTAB WHERE TANA = 'AFT' AND FINA IN ('ELDN','ALDN','EIBN','AIBN','EOBN','AOBN','ETON','ATON');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'AFTTAB' 
and a.column_name IN ('ELDN','ALDN','EIBN','AIBN','EOBN','AOBN','ETON','ATON');

COMMIT;