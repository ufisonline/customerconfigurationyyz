alter table fshtab add RSTY VARCHAR2(20)	default ' ';
COMMENT ON COLUMN FSHTAB.RSTY IS 'Reference Info Sub-Type';

DELETE SYSTAB WHERE TANA = 'FSH' AND FINA = 'RSTY';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FSHTAB' 
and a.column_name = 'RSTY';

COMMIT;

ALTER TABLE CFG_MSG_TEMPLATE ADD (
text_format	VARCHAR2(20)	,
mandatory_flag	CHAR(1)	DEFAULT ' ',
md_reftab		VARCHAR2(32),
md_reffld	VARCHAR2(32)
);

COMMENT ON COLUMN CFG_MSG_TEMPLATE.text_format IS 'i.e. YYYYMMDD';
COMMENT ON COLUMN CFG_MSG_TEMPLATE.mandatory_flag	IS 'Y - mandatory blank - otherwise';
COMMENT ON COLUMN CFG_MSG_TEMPLATE.md_reftab	IS 'Master Data reference';
COMMENT ON COLUMN CFG_MSG_TEMPLATE.md_reffld	IS 'Master Data reference field';

alter table cfg_msg_template modify text_type varchar2(12);

COMMIT;