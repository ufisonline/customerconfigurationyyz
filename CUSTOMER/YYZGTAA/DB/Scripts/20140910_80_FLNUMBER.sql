CREATE TABLE FNMTAB (  
URNO NUMBER(10,0) default 0 PRIMARY KEY,
CDAT CHAR(14) default ' ' ,
LSTU CHAR(14) default ' ' ,
USEC VARCHAR2(32) default ' ' ,
USEU VARCHAR2(32) default ' ' ,
FSNO NUMBER default 0 ,
FENO NUMBER default 0 ,
SALC CHAR(3) default ' ' ,
MALC CHAR(3) default ' ' ,
TYPE VARCHAR2(64) default ' '
 )
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (    
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1    
  )    
  TABLESPACE "CEDA02" ;    


CREATE INDEX FNMTAB_FSNO ON FNMTAB
    (
      FSNO
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 10485760 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 
    )
    TABLESPACE CEDA02 ;


CREATE INDEX FNMTAB_FENO ON FNMTAB
    (
      FENO
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 10485760 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 
    )
    TABLESPACE CEDA02 ;

CREATE INDEX FNMTAB_SALC ON FNMTAB
    (
      SALC
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 10485760 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 
    )
    TABLESPACE CEDA02 ;

CREATE INDEX FNMTAB_TYPE ON FNMTAB
    (
      TYPE
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 10485760 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 
    )
    TABLESPACE CEDA02 ;

COMMENT ON COLUMN FNMTAB.URNO IS 'Unique Record Number';
COMMENT ON COLUMN FNMTAB.CDAT IS 'Record created date time';
COMMENT ON COLUMN FNMTAB.LSTU IS 'Record last update date time';
COMMENT ON COLUMN FNMTAB.USEC IS 'User whom created the record';
COMMENT ON COLUMN FNMTAB.USEU IS 'User whom updated the record';
COMMENT ON COLUMN FNMTAB.FSNO IS 'Flight Number Range - start';
COMMENT ON COLUMN FNMTAB.FENO IS 'Flight Number Range - end';
COMMENT ON COLUMN FNMTAB.SALC IS 'Source Airline Code';
COMMENT ON COLUMN FNMTAB.MALC IS 'Mapped Airline Code';
COMMENT ON COLUMN FNMTAB.TYPE IS 'Mapping Type';


DELETE SYSTAB WHERE TANA = 'FNM';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FNMTAB';

COMMIT;

CREATE TABLE FMATAB (  
URNO NUMBER(10,0) default 0 ,
CDAT CHAR(14) default ' ' ,
LSTU CHAR(14) default ' ' ,
USEC VARCHAR2(32) default ' ' ,
USEU VARCHAR2(32) default ' ' ,
STAN CHAR(3) default ' ' ,
SFIN CHAR(4) default ' ' ,
SFVA VARCHAR2(64) default ' ' ,
MTAN CHAR(3) default ' ',
MFIN CHAR(4) default ' ',
PRIMARY KEY (URNO)
)
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (    
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1    
  )    
  TABLESPACE "CEDA02" ;    

CREATE INDEX FMATAB_STAN ON FMATAB
    (
      STAN
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 10485760 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 
    )
    TABLESPACE CEDA02 ;

CREATE INDEX FMATAB_SFIN ON FMATAB
    (
      SFIN
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 10485760 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 
    )
    TABLESPACE CEDA02 ;

COMMENT ON COLUMN FMATAB.URNO IS 'Unique Record Number';
COMMENT ON COLUMN FMATAB.CDAT IS 'Record created date time';
COMMENT ON COLUMN FMATAB.LSTU IS 'Record last update date time';
COMMENT ON COLUMN FMATAB.USEC IS 'User whom created the record';
COMMENT ON COLUMN FMATAB.USEU IS 'User whom updated the record';
COMMENT ON COLUMN FMATAB.STAN IS 'Flight Number Range - start';
COMMENT ON COLUMN FMATAB.SFIN IS 'Flight Number Range - end';
COMMENT ON COLUMN FMATAB.SFVA IS 'Source Airline Code';
COMMENT ON COLUMN FMATAB.MTAN IS 'Mapped Airline Code';
COMMENT ON COLUMN FMATAB.MFIN IS 'Mapping Type';


DELETE SYSTAB WHERE TANA = 'FMA';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FMATAB';

COMMIT;


ALTER TABLE AFTTAB ADD PFNO VARCHAR(9) ;
ALTER TABLE AFTTAB MODIFY PFNO DEFAULT ' ' ;
ALTER TABLE AFTTAB ADD PALP VARCHAR(9) ;
ALTER TABLE AFTTAB MODIFY PALP DEFAULT ' ' ;
ALTER TABLE AFTTAB ADD PALB VARCHAR(9) ;
ALTER TABLE AFTTAB MODIFY PALB DEFAULT ' ' ;
ALTER TABLE AFTTAB ADD PALL VARCHAR(9) ;
ALTER TABLE AFTTAB MODIFY PALL DEFAULT ' ' ;
ALTER TABLE AFTTAB ADD PALN VARCHAR(9) ;
ALTER TABLE AFTTAB MODIFY PALN DEFAULT ' ' ;


COMMENT ON COLUMN AFTTAB.PFNO IS 'Public display Flight Number';
COMMENT ON COLUMN AFTTAB.PALP IS 'Public Display Airline';
COMMENT ON COLUMN AFTTAB.PALB IS 'Public Display Airline for Baggage';
COMMENT ON COLUMN AFTTAB.PALL IS 'Public Display Airline for Logo';
COMMENT ON COLUMN AFTTAB.PALN IS 'Public Display Airline for Name';

DELETE SYSTAB WHERE TANA = 'AFT' AND FINA IN ('PFNO','PALP','PALB','PALL','PALN');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'AFTTAB'
and a.column_name IN ('PFNO','PALP','PALB','PALL','PALN');

COMMIT;
