--------------------------------------------------------
--  File created - Tuesday-May-12-2015   
--------------------------------------------------------
DROP TABLE "CEDA"."GSCTAB";
--------------------------------------------------------
--  DDL for Table GSCTAB
--------------------------------------------------------

  CREATE TABLE "CEDA"."GSCTAB" 
   (	"URNO" VARCHAR2(10 BYTE) DEFAULT ' ', 
	"GNAM" VARCHAR2(10 BYTE) DEFAULT ' ', 
	"FLTI" VARCHAR2(64 BYTE) DEFAULT ' ', 
	"FLTO" VARCHAR2(64 BYTE) DEFAULT ' ', 
	"USEC" CHAR(32 BYTE) DEFAULT ' ', 
	"USEU" CHAR(32 BYTE) DEFAULT ' ', 
	"CDAT" CHAR(14 BYTE) DEFAULT ' ', 
	"LSTU" CHAR(14 BYTE) DEFAULT ' ', 
	"HOPO" CHAR(3 BYTE) DEFAULT 'YYZ'
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CEDA02" ;
 

   COMMENT ON COLUMN "CEDA"."GSCTAB"."URNO" IS 'ID';
 
   COMMENT ON COLUMN "CEDA"."GSCTAB"."GNAM" IS 'Gate Name (GAT.GNAM)';
 
   COMMENT ON COLUMN "CEDA"."GSCTAB"."FLTI" IS 'Possible Incoming (Arrival) Flight Sector IDs at particular time frame (without swing operation)';
 
   COMMENT ON COLUMN "CEDA"."GSCTAB"."FLTO" IS 'Possible Outgoing (Departure) Flight Sector IDs at particular time frame (without swing operation)';
REM INSERTING into CEDA.GSCTAB
SET DEFINE OFF;
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('346587417','01','I/S','I','UFIS$ADMIN                      ','UFIS$ADMIN                      ','20150327095840','20150409083157','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('382700870','01A',' ','S','UFIS$ADMIN                      ','                                ','20150409082525','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('43776976','01','S/I','S','UFIS$ADMIN                      ','UFIS$ADMIN                      ','20150406082834','20150409083219','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('382700869','01A',' ','I','UFIS$ADMIN                      ','                                ','20150409082505','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376807','01B','I',' ','UFIS$ADMIN                      ','                                ','20150409083012','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376814','02','S/I','S','UFIS$ADMIN                      ','38783                           ','20150409083402','20150424073439','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376817','02B','S',null,'UFIS$ADMIN                      ','UFIS$ADMIN                      ','20150409083518','20150507083131','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376820','03','S',' ','UFIS$ADMIN                      ','                                ','20150409083618','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376827','03A',' ','S','UFIS$ADMIN                      ','                                ','20150409083740','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376832','04A',' ','S','UFIS$ADMIN                      ','                                ','20150409083908','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('384376837','04','S','S','UFIS$ADMIN                      ','                                ','20150409084025','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('385653340','05','S','S','UFIS$ADMIN                      ','                                ','20150409084552','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('385653342','05B','S',' ','UFIS$ADMIN                      ','                                ','20150409084736','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('385653347','06','S','S','UFIS$ADMIN                      ','                                ','20150409084845','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('326000902','07A',' ','S','94483                           ','                                ','20150420114712','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('326000903','07A',' ','I','94483                           ','                                ','20150420114719','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('441014980','03',' ','S','94483                           ','                                ','20150421122524','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('441014984','07','S/I','S','94483                           ','                                ','20150421123216','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('441014985','07','I/S','I','94483                           ','                                ','20150421123228','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('441040692','07B','I',' ','94483                           ','94483                           ','20150421123450','20150421123457','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442587445','07B','S',' ','94483                           ','                                ','20150421123503','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442587448','08','S/I','I','94483                           ','                                ','20150421123622','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442587449','08','I/S','S','94483                           ','                                ','20150421123637','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442587461','09','S/I','I','94483                           ','94483                           ','20150421123742','20150421123801','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442587462','09','I/S','S','94483                           ','                                ','20150421123752','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442863002','09A',null,'I','94483                           ','94483                           ','20150421124619','20150421124704','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442863057','09A',' ','S','94483                           ','                                ','20150421124709','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442863058','10B','I',' ','94483                           ','                                ','20150421124754','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('442863059','10B','S',' ','94483                           ','                                ','20150421124800','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440366','10A',' ','I','94483                           ','                                ','20150421124906','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440367','10A','I','S','94483                           ','UFIS$ADMIN                      ','20150421124912','20150429031304','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440371','10','I/S','I','94483                           ','                                ','20150421125002','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440372','10','S/I','S','94483                           ','                                ','20150421125012','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440377','11','S/I','I','94483                           ','94483                           ','20150421125221','20150421125238','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440378','11','I/S','S','94483                           ','                                ','20150421125230','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440384','11B','I',' ','94483                           ','                                ','20150421125458','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440386','11B','S',' ','94483                           ','                                ','20150421125503','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440399','12A',' ','I','94483                           ','                                ','20150421125715','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440400','12A',' ','S','94483                           ','                                ','20150421125720','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440404','12','I/S','I','94483                           ','                                ','20150421125807','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('445440405','12','S/I','S','94483                           ','                                ','20150421125814','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448496394','12D','S',' ','94483                           ','                                ','20150421131716','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448496401','13','S/I','S','94483                           ','94483                           ','20150421132046','20150422084502','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('738111473','15','S/I','S','94483                           ','                                ','20150422084749','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448496405','13B',' ','I','94483                           ','                                ','20150421132234','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448496406','13B',' ','S','94483                           ','                                ','20150421132241','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448506729','16A',' ','I','94483                           ','                                ','20150421132728','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448506730','16A',' ','S','94483                           ','                                ','20150421132733','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448506734','17A',' ','I','94483                           ','                                ','20150421132830','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('448506735','17A',' ','S','94483                           ','                                ','20150421132834','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('738111472','14','S/I','S','94483                           ','                                ','20150422084704','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742013366','15B','I/S',' ','94483                           ','                                ','20150422084926','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742013373','16','S/I','S','94483                           ','                                ','20150422085023','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742563429','17','S/I','S','94483                           ','                                ','20150422085351','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742563433','18','S/I','S','94483                           ','                                ','20150422085436','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742563434','18','S/I','I','94483                           ','                                ','20150422085447','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742563439','18B','S/I',' ','94483                           ','                                ','20150422085700','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742563470','19','I/S','S','94483                           ','                                ','20150422085745','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('742563471','19','S/I','I','94483                           ','                                ','20150422085752','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('744499252','20B','I/S',' ','94483                           ','                                ','20150422085836','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('744499265','20','I/S','I','94483                           ','                                ','20150422085928','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('744499266','20','I/S','S','94483                           ','                                ','20150422085936','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('744499293','21',' ','I/S','94483                           ','                                ','20150422090111','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('751252531','D',' ','I/S','94483                           ','                                ','20150422091459','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('1250253285','02I',' ','I','38783                           ','                                ','20150424072833','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('1250253313','01S',' ','S','38783                           ','                                ','20150424073307','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('666868509','01B','S/I',null,'UFIS$ADMIN                      ','                                ','20150429025613','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('1454914054','01','S','I','UFIS$ADMIN                      ','                                ','20150507094422','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('1454914057','01E','I',null,'UFIS$ADMIN                      ','                                ','20150507095234','              ','YYZ');
Insert into CEDA.GSCTAB (URNO,GNAM,FLTI,FLTO,USEC,USEU,CDAT,LSTU,HOPO) values ('1454914058','01E','S/I',null,'UFIS$ADMIN                      ','                                ','20150507095235','              ','YYZ');
--------------------------------------------------------
--  DDL for Index SYS_C0019828
--------------------------------------------------------

  CREATE UNIQUE INDEX "CEDA"."SYS_C0019828" ON "CEDA"."GSCTAB" ("URNO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CEDA01" ;
--------------------------------------------------------
--  Constraints for Table GSCTAB
--------------------------------------------------------

  ALTER TABLE "CEDA"."GSCTAB" ADD PRIMARY KEY ("URNO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CEDA01"  ENABLE;
