CREATE TABLE Rule_Conflict 
(
id VARCHAR(40) DEFAULT ' ',
id_rule_detail VARCHAR(40) DEFAULT	' ',
tbl_ref VARCHAR(200) DEFAULT	' ',
id_ref VARCHAR(200) DEFAULT ' ',
msg_type VARCHAR(40) DEFAULT ' ',
msg VARCHAR(500) DEFAULT	' ',
rec_status CHAR(1) DEFAULT ' ',
id_hopo	VARCHAR2(4) DEFAULT	' ',
data_source	VARCHAR2(8) DEFAULT	Null,
created_date	DATE DEFAULT SYSDATE,
updated_date	DATE DEFAULT Null,
created_user	VARCHAR2(32) DEFAULT Null,
updated_user	VARCHAR2(32) DEFAULT Null,
opt_lock NUMBER(22,0) DEFAULT	0
);
COMMENT ON COLUMN Rule_Conflict.id IS 'ID';
COMMENT ON COLUMN Rule_Conflict.id_rule_detail IS 'ID of rule Rule_Detail';
COMMENT ON COLUMN Rule_Conflict.tbl_ref IS 'Reference Table Name';
COMMENT ON COLUMN Rule_Conflict.id_ref IS 'Id of Reference Table';
COMMENT ON COLUMN Rule_Conflict.msg_type IS 'Message Type �C� � Conflict Message �W� � Warning Message ';
COMMENT ON COLUMN Rule_Conflict.msg IS 'Message';
COMMENT ON COLUMN Rule_Conflict.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Rule_Conflict.id_hopo IS 'Home Airport';
COMMENT ON COLUMN Rule_Conflict.data_source IS 'Data Source';
COMMENT ON COLUMN Rule_Conflict.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Rule_Conflict.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Rule_Conflict.created_user IS 'User or process which created this record';
COMMENT ON COLUMN Rule_Conflict.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN Rule_Conflict.opt_lock IS 'Optimistic Lock';
  
  
  
  
 