alter table atrtab add qcod char(4) default ' ';

comment on column atrtab.qcod is 'Message QCode';

DELETE SYSTAB WHERE TANA = 'ATR' and FINA = 'QCOD';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO)
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX"
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b
where a.table_name (+) = b.table_name
and a.column_name (+) = b.column_name
and a.table_name = 'ATRTAB'
and a.column_name = 'QCOD';

commit;


alter table "ATRTAB" add ( "NUMR" CHAR(8) );
alter table "ATRTAB" add ( "YEAR" CHAR(2) );
alter table "ATRTAB" add ( "CLAS" CHAR(1) );
alter table "ATRTAB" add ( "SNDU" CHAR(8) );
alter table "ATRTAB" add ( "SNDT" CHAR(14) );
alter table "ATRTAB" add ( "SRES" CHAR(1) );
COMMENT ON TABLE ATRTAB IS 'Air Traffic Restriction' ;
COMMENT ON COLUMN "ATRTAB"."USEC" IS 'User who has created the record' ;

DELETE SYSTAB WHERE TANA = 'ATR' and FINA in ('NUMR','YEAR','CLAS','SNDU','SNDT','SRES');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'ATRTAB' 
and a.column_name IN ('NUMR','YEAR','CLAS','SNDU','SNDT','SRES');

commit;