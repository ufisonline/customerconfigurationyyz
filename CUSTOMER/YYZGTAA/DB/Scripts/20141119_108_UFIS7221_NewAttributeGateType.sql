
/***
-- Script for "GATE TYPE RULE" which will be defined as system default rule
***/

-- SYS_DOMAINATTRIBUTE
DELETE FROM SYS_DOMAINATTRIBUTE WHERE ID = '61';
INSERT INTO SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,REC_STATUS,ID_HOPO,OPT_LOCK) 
VALUES ('61','1','GateType',3,'GATTAB','GTYP',' ','YYZ',0);



-- SYS_DATALINK
DELETE FROM SYS_DATALINK WHERE ID = '3';
INSERT INTO SYS_DATALINK (ID,RESULT_TYPE,FR_TABLE_NAME,TO_TABLE_NAME,JOIN_CLAUSE,
DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,
UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK,TABLES_USED) 
values ('3',1,'RALTAB','GATTAB','RALTAB.RESC = ''GAT'' AND RALTAB.RESD = TRIM(GATTAB.GNAM)',
' ',' ',' ',null,to_date('17/11/14','DD/MM/RR'),
null,null,null,0,'GATTAB');


-- RULE_DETAIL
DELETE FROM CEDA.RULE_DETAIL WHERE ID = '01407dbb-cfc8-485d-a514-3e9c87c59b6d';
Insert into CEDA.RULE_DETAIL (ID,NAME,DESCR,RES_TYPE,RULE_TYPE,DEF_SCORE,MSG,CLIENT_RULE_CRITERIA1,CLIENT_RULE_CRITERIA2,SERVER_RULE_CRITERIA1,SERVER_RULE_CRITERIA2,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('01407dbb-cfc8-485d-a514-3e9c87c59b6d','ALLOWED GATE TYPE','Allowed gate type system default rule','GAT',-1,-500,'Allowed gate type rule','{"RuleDomainObjects":[{"ItemId":"1""Order":1}]"RuleCriteriaString":"Not IsNullOrEmpty([$ResourceAlloc1.GateType]) Or [$ResourceAlloc1.AllocationType] = ''0'' And Not [$ResourceAlloc1.GateType] In (''B'') Or [$ResourceAlloc1.AllocationType] = ''1'' And Not [$ResourceAlloc1.GateType] In (''A'' ''B'') Or [$ResourceAlloc1.AllocationType] = ''2'' And Not [$ResourceAlloc1.GateType] In (''D'' ''B'') Or [$ResourceAlloc1.ResourceCategory] = ''GAT''"}',' ','$ResourceAlloc1:ResourceAlloc((($ResourceAlloc1.GateType != null &&$ResourceAlloc1.GateType != '''') &&($ResourceAlloc1.ResourceCategory == ''GAT'')) &&(($ResourceAlloc1.AllocationType == ''0'' &&$ResourceAlloc1.GateType not in (''B'')) || ($ResourceAlloc1.AllocationType == ''1'' &&$ResourceAlloc1.GateType not in (''A'' ''B'')) || ($ResourceAlloc1.AllocationType == ''2'' &&$ResourceAlloc1.GateType not in (''D'' ''B''))))',' ',' ','YYZ',null,to_date('17/11/14','DD/MM/RR'),to_date('18/11/14','DD/MM/RR'),'UFIS$ADMIN','UFIS$ADMIN',0);


commit;