-- =====================================================================
-- Create Table
-- =====================================================================



CREATE TABLE "SCNTAB" (
  "SCCD"	CHAR(10)	NOT NULL,
  "SCNM"	CHAR(50)	NOT NULL
) 
TABLESPACE "CEDA02";

COMMENT ON "SCNTAB" ( 
	"SCCD" IS 'Screen Code',
	"SCNM" IS 'Screen Name' );

COMMIT;



CREATE TABLE "COLTAB" (
  "SEQN"	NUMBER	NOT NULL,
  "SCCD"	CHAR(10)	NOT NULL,
  "CONM"	CHAR(20)	NOT NULL,
  "COID"	CHAR(30)	NOT NULL,
  "CODS"	CHAR(50)	NOT NULL
) 
TABLESPACE "CEDA02";


COMMENT ON "COLTAB" ( 
	"SEQN" IS 'Column Sequence',
	"SCCD" IS 'Screen Code from SCNTAB',
	"CONM" IS 'Column Name that display on FIPS screen',
	"COID" IS 'Unique Column Name',
	"CODS" IS 'Column Description' );



CREATE TABLE "UCOTAB" (
  "URNO"	NUMBER	NOT NULL,
  "USID"	CHAR(20)	NOT NULL,
  "TYPE"	CHAR(10)	NOT NULL,
  "SCCD"	CHAR(10)	NOT NULL,
  "SEQL"	CHAR(200)
) 
TABLESPACE "CEDA02";


COMMENT ON "UCOTAB" ( 
	"URNO" IS 'Urno',
	"USID" IS 'User ID',
	"TYPE" IS 'Setup Type',
	"SCCD" IS 'Screen Code from SCNTAB',
	"SEQL" IS 'Column Sequence List from COLTAB' );


-- =====================================================================
-- Add to SYSTAB to be Basic Data (*** require ***)
Please check existing URNO before insert
-- =====================================================================

insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Urno',' ','. ',' ',' ','10','URNO','N',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Urno',' ',' ',' ','TAB','. ','Y',' ',' ','N','UCO','UCO','LONG','8001','N',' ','Urno',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('User ID',' ','. ',' ',' ','20','USID','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','User ID',' ',' ',' ','TAB','. ','Y',' ',' ','N','UCO','UCO','TRIM','8002','N',' ','User ID',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Setup Type',' ','. ',' ',' ','10','TYPE','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Setup Type',' ',' ',' ','TAB','. ','Y',' ',' ','N','UCO','UCO','TRIM','8003','N',' ','Setup Type',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Screen Code',' ','. ',' ',' ','10','SCCD','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Screen Code',' ',' ',' ','TAB','. ','Y',' ',' ','N','UCO','UCO','TRIM','8004','N',' ','Screen Code',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Column Seq List',' ','. ',' ',' ','200','SEQL','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Column Seq List',' ',' ',' ','TAB','. ','N',' ',' ','N','UCO','UCO','TRIM','8005','N',' ','Column Seq List',' ',' ',' ','N',' ');
COMMIT;

insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Column Seq',' ','. ',' ',' ','5','SEQN','N',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Column Seq',' ',' ',' ','TAB','. ','Y',' ',' ','N','COL','COL','TRIM','8006','N',' ','Column Seq',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Screen Code',' ','. ',' ',' ','10','SCCD','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Screen Code',' ',' ',' ','TAB','. ','Y',' ',' ','N','COL','COL','TRIM','8007','N',' ','Screen Code',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Column Name',' ','. ',' ',' ','20','CONM','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Column Name',' ',' ',' ','TAB','. ','Y',' ',' ','N','COL','COL','TRIM','8008','N',' ','Column Name',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Unique Column Name',' ','. ',' ',' ','30','COID','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Unique Column Name',' ',' ',' ','TAB','. ','Y',' ',' ','N','COL','COL','TRIM','8009','N',' ','Unique Column Name',' ',' ',' ','N',' ');
insert into SYSTAB (ADDI,CONT,DATR,DEFA,DEPE,FELE,FINA,FITY,FMTS,GCFL,GDSR,GLFL,GSTY,GTYP,GUID,HOPO,INDX,KONT,LABL,LOGD,MSGT,ORIE,PROJ,REFE,REQF,SORT,STAT,SYST,TANA,TATY,TYPE,URNO,CLSR,DEFV,DSCR,FTAN,HEDR,TOLT,TTYP,TZON) VALUES ('Column Description',' ','. ',' ',' ','50','CODS','C',' ',' ',' ',' ',' ',' ',' ','YYZ','N',' ','Column Description',' ',' ',' ','TAB','. ','N',' ',' ','N','COL','COL','TRIM','8010','N',' ','Column Description',' ',' ',' ','N',' ');
COMMIT;


-- =====================================================================
-- Populate Default Value
-- =====================================================================

INSERT INTO SCNTAB VALUES('DSAR','Daily Schedule - Arrival');
COMMIT;

INSERT INTO COLTAB VALUES(601,'DSAR','Flight','A_Flight_','Flight Number');
INSERT INTO COLTAB VALUES(602,'DSAR','C/S','A_C/S_','Call Sign');
INSERT INTO COLTAB VALUES(603,'DSAR','ORG','A_ORG_','Origin Airport');
INSERT INTO COLTAB VALUES(604,'DSAR','VIA','A_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(605,'DSAR',' ','A_VIAN_','Number of Via Airport');
INSERT INTO COLTAB VALUES(606,'DSAR','A','A_A_','Arrival Type (IFR/VFR)');
INSERT INTO COLTAB VALUES(607,'DSAR','Na','A_Na_','Flight Nature');
INSERT INTO COLTAB VALUES(608,'DSAR','SIBT','A_SIBT_','Schedule In-Block Time');
INSERT INTO COLTAB VALUES(609,'DSAR','ELDT','A_ELDT_','Estimated Landing Time');
INSERT INTO COLTAB VALUES(610,'DSAR','TLDT','A_TLDT_','Target Landing Time');
INSERT INTO COLTAB VALUES(611,'DSAR','TMO','A_TMO_','Twenty Mile');
INSERT INTO COLTAB VALUES(612,'DSAR','ALDT','A_ALDT_','Actual Landing Time');
INSERT INTO COLTAB VALUES(613,'DSAR','RWY','A_RWY_','Runway');
INSERT INTO COLTAB VALUES(614,'DSAR','EIBT','A_EIBT_','Estimated In-Block Time');
INSERT INTO COLTAB VALUES(615,'DSAR','AIBT','A_AIBT_','Actual In-Block Time');
INSERT INTO COLTAB VALUES(616,'DSAR','POS','A_POS_','Position');
INSERT INTO COLTAB VALUES(617,'DSAR','Gate','A_Gate_','Gate');
INSERT INTO COLTAB VALUES(618,'DSAR','Belt','A_Belt_','Belt');
INSERT INTO COLTAB VALUES(619,'DSAR','REG','A_REG_','Registration');
INSERT INTO COLTAB VALUES(620,'DSAR','A/C','A_A/C_','Aircraft Type');
INSERT INTO COLTAB VALUES(621,'DSAR','Date SIBT','A_Date SIBT_','Date of SIBT');
INSERT INTO COLTAB VALUES(622,'DSAR','I','A_I_','Remark Flag');
INSERT INTO COLTAB VALUES(623,'DSAR','V','A_V_','Number of VIP');
INSERT INTO COLTAB VALUES(624,'DSAR','REM','A_REM_','Remark');
INSERT INTO COLTAB VALUES(625,'DSAR','Rem. 1 ARR Daily','A_Rem. 1 ARR Daily_','Arrival Remark 1 for Daily Schedule');
INSERT INTO COLTAB VALUES(626,'DSAR','Rem. ARR Seasonal','A_Rem. ARR Seasonal_','Arrival Remark for Seasonal Schedule');
COMMIT;

INSERT INTO SCNTAB VALUES('DSAG','Daily Schedule - Arrival Ground');
COMMIT;

INSERT INTO COLTAB VALUES(101,'DSAG','Flight','A_Flight_','Flight Number');
INSERT INTO COLTAB VALUES(102,'DSAG','C/S','A_C/S_','Call Sign');
INSERT INTO COLTAB VALUES(103,'DSAG','ORG','A_ORG_','Origin Airport');
INSERT INTO COLTAB VALUES(104,'DSAG','VIA','A_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(105,'DSAG',' ','A_VIAN_','Number of Via Airport');
INSERT INTO COLTAB VALUES(106,'DSAG','A','A_A_','Arrival Type (IFR/VFR)');
INSERT INTO COLTAB VALUES(107,'DSAG','Na','A_Na_','Flight Nature');
INSERT INTO COLTAB VALUES(108,'DSAG','SIBT','A_SIBT_','Schedule In-Block Time');
INSERT INTO COLTAB VALUES(109,'DSAG','ELDT','A_ELDT_','Estimated Landing Time');
INSERT INTO COLTAB VALUES(110,'DSAG','TLDT','A_TLDT_','Target Landing Time');
INSERT INTO COLTAB VALUES(111,'DSAG','TMO','A_TMO_','Twenty Mile');
INSERT INTO COLTAB VALUES(112,'DSAG','ALDT','A_ALDT_','Actual Landing Time');
INSERT INTO COLTAB VALUES(113,'DSAG','RWY','A_RWY_','Runway');
INSERT INTO COLTAB VALUES(114,'DSAG','EIBT','A_EIBT_','Estimated In-Block Time');
INSERT INTO COLTAB VALUES(115,'DSAG','AIBT','A_AIBT_','Actual In-Block Time');
INSERT INTO COLTAB VALUES(116,'DSAG','POS','A_POS_','Position');
INSERT INTO COLTAB VALUES(117,'DSAG','Gate','A_Gate_','Gate');
INSERT INTO COLTAB VALUES(118,'DSAG','Belt','A_Belt_','Belt');
INSERT INTO COLTAB VALUES(119,'DSAG','REG','A_REG_','Registration');
INSERT INTO COLTAB VALUES(120,'DSAG','A/C','A_A/C_','Aircraft Type');
INSERT INTO COLTAB VALUES(121,'DSAG','Date ALDT','A_Date ALDT_','Date of ALDT');
INSERT INTO COLTAB VALUES(122,'DSAG','I','A_I_','Remark Flag');
INSERT INTO COLTAB VALUES(123,'DSAG','V','A_V_','Number of VIP');
INSERT INTO COLTAB VALUES(124,'DSAG','REM','A_REM_','Remark');
INSERT INTO COLTAB VALUES(125,'DSAG','Rem. 1 ARR Daily','A_Rem. 1 ARR Daily_','Arrival Remark 1 for Daily Schedule');
INSERT INTO COLTAB VALUES(126,'DSAG','Rem. ARR Seasonal','A_Rem. ARR Seasonal_','Arrival Remark for Seasonal Schedule');
COMMIT;

INSERT INTO SCNTAB VALUES('DSDG','Daily Schedule - Departure Ground');
COMMIT;

INSERT INTO COLTAB VALUES(201,'DSDG','Flight','D_Flight_','Flight Number');
INSERT INTO COLTAB VALUES(202,'DSDG','C/S','D_C/S_','Call Sign');
INSERT INTO COLTAB VALUES(203,'DSDG','DES','D_DES_','Dstination Airport');
INSERT INTO COLTAB VALUES(204,'DSDG','VIA','D_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(205,'DSDG',' ','D_VIAN_','Number of Via Airport');
INSERT INTO COLTAB VALUES(206,'DSDG','A','D_A_','Arrival Type (IFR/VFR)');
INSERT INTO COLTAB VALUES(207,'DSDG','Na','D_Na_','Flight Nature');
INSERT INTO COLTAB VALUES(208,'DSDG','SOBT','D_SOBT_','Schedule Off-Block Time');
INSERT INTO COLTAB VALUES(209,'DSDG','EOBT','D_EOBT_','Estimated Off-Block Time');
INSERT INTO COLTAB VALUES(210,'DSDG','TOBT','D_TOBT_','Target Off-Block Time');
INSERT INTO COLTAB VALUES(211,'DSDG','TSAT','D_TSAT_','Target Start Up Approval Time');
INSERT INTO COLTAB VALUES(212,'DSDG','ASRT','D_ASRT_','Actual Start Up Request Time');
INSERT INTO COLTAB VALUES(213,'DSDG','ASAT','D_ASAT_','Actual Start Up Approval Time');
INSERT INTO COLTAB VALUES(214,'DSDG','AOBT','D_AOBT_','Actual Off-Block Time');
INSERT INTO COLTAB VALUES(215,'DSDG','CTOT','D_CTOT_','Calculated Take Off Time');
INSERT INTO COLTAB VALUES(216,'DSDG','RWY','D_RWY_','Runway');
INSERT INTO COLTAB VALUES(217,'DSDG','TTOT','D_TTOT_','Target Take Off Time');
INSERT INTO COLTAB VALUES(218,'DSDG','ATOT','D_ATOT_','Actual Take Off Time');
INSERT INTO COLTAB VALUES(219,'DSDG','POS','D_POS_','Position');
INSERT INTO COLTAB VALUES(220,'DSDG','Gate1','D_Gate1_','Gate 1');
INSERT INTO COLTAB VALUES(221,'DSDG','Gate2','D_Gate2_','Gate 2');
INSERT INTO COLTAB VALUES(222,'DSDG','Cki','D_Cki_','Check-In Counter');
INSERT INTO COLTAB VALUES(223,'DSDG','REG','D_REG_','Registration');
INSERT INTO COLTAB VALUES(224,'DSDG','A/C','D_A/C_','Aircraft Type');
INSERT INTO COLTAB VALUES(225,'DSDG','Date SOBT','D_Date SOBT_','Date of SOBT');
INSERT INTO COLTAB VALUES(226,'DSDG','I','D_I_','Remark Flag');
INSERT INTO COLTAB VALUES(227,'DSDG','V','D_V_','Number of VIP');
INSERT INTO COLTAB VALUES(228,'DSDG','REM','D_REM_','Remark');
INSERT INTO COLTAB VALUES(229,'DSDG','Rem. 1 DEP Daily','D_Rem. 1 DEP Daily_','Departure Remark 1 for Daily Schedule');
INSERT INTO COLTAB VALUES(230,'DSDG','Rem. DEP Seasonal','D_Rem. DEP Seasonal_','Departure Remark for Seasonal Schedule');
COMMIT;

INSERT INTO SCNTAB VALUES('DSDE','Daily Schedule - Departure');
COMMIT;

INSERT INTO COLTAB VALUES(301,'DSDE','Flight','D_Flight_','Flight Number');
INSERT INTO COLTAB VALUES(302,'DSDE','C/S','D_C/S_','Call Sign');
INSERT INTO COLTAB VALUES(303,'DSDE','DES','D_DES_','Dstination Airport');
INSERT INTO COLTAB VALUES(304,'DSDE','VIA','D_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(305,'DSDE',' ','D_VIAN_','Number of Via Airport');
INSERT INTO COLTAB VALUES(306,'DSDE','A','D_A_','Arrival Type (IFR/VFR)');
INSERT INTO COLTAB VALUES(307,'DSDE','Na','D_Na_','Flight Nature');
INSERT INTO COLTAB VALUES(308,'DSDE','SOBT','D_SOBT_','Schedule Off-Block Time');
INSERT INTO COLTAB VALUES(309,'DSDE','EOBT','D_EOBT_','Estimated Off-Block Time');
INSERT INTO COLTAB VALUES(310,'DSDE','TOBT','D_TOBT_','Target Off-Block Time');
INSERT INTO COLTAB VALUES(311,'DSDE','TSAT','D_TSAT_','Target Start Up Approval Time');
INSERT INTO COLTAB VALUES(312,'DSDE','ASRT','D_ASRT_','Actual Start Up Request Time');
INSERT INTO COLTAB VALUES(313,'DSDE','ASAT','D_ASAT_','Actual Start Up Approval Time');
INSERT INTO COLTAB VALUES(314,'DSDE','AOBT','D_AOBT_','Actual Off-Block Time');
INSERT INTO COLTAB VALUES(315,'DSDE','CTOT','D_CTOT_','Calculated Take Off Time');
INSERT INTO COLTAB VALUES(316,'DSDE','RWY','D_RWY_','Runway');
INSERT INTO COLTAB VALUES(317,'DSDE','TTOT','D_TTOT_','Target Take Off Time');
INSERT INTO COLTAB VALUES(318,'DSDE','ATOT','D_ATOT_','Actual Take Off Time');
INSERT INTO COLTAB VALUES(319,'DSDE','POS','D_POS_','Position');
INSERT INTO COLTAB VALUES(320,'DSDE','Gate1','D_Gate1_','Gate 1');
INSERT INTO COLTAB VALUES(321,'DSDE','Gate2','D_Gate2_','Gate 2');
INSERT INTO COLTAB VALUES(322,'DSDE','Cki','D_Cki_','Check-In Counter');
INSERT INTO COLTAB VALUES(323,'DSDE','REG','D_REG_','Registration');
INSERT INTO COLTAB VALUES(324,'DSDE','A/C','D_A/C_','Aircraft Type');
INSERT INTO COLTAB VALUES(325,'DSDE','Date ATOT','D_Date ATOT_','Date of ATOT');
INSERT INTO COLTAB VALUES(326,'DSDE','I','D_I_','Remark Flag');
INSERT INTO COLTAB VALUES(327,'DSDE','V','D_V_','Number of VIP');
INSERT INTO COLTAB VALUES(328,'DSDE','REM','D_REM_','Remark');
INSERT INTO COLTAB VALUES(329,'DSDE','Rem. 1 DEP Daily','D_Rem. 1 DEP Daily_','Departure Remark 1 for Daily Schedule');
INSERT INTO COLTAB VALUES(330,'DSDE','Rem. DEP Seasonal','D_Rem. DEP Seasonal_','Departure Remark for Seasonal Schedule');
COMMIT;

INSERT INTO SCNTAB VALUES('FSCH','Flight Schedule');
COMMIT;

INSERT INTO COLTAB VALUES(401,'FSCH','Arrival','A_Arrival_','Arrival Flight Number');
INSERT INTO COLTAB VALUES(402,'FSCH','ID','A_ID_','Inter/Domestic');
INSERT INTO COLTAB VALUES(403,'FSCH','J','A_J_','Number of Codeshared Flight');
INSERT INTO COLTAB VALUES(404,'FSCH','Date','A_Date_','Schedule In-Block Date');
INSERT INTO COLTAB VALUES(405,'FSCH','SIBT','A_SIBT_','Schedule In-Block Time');
INSERT INTO COLTAB VALUES(406,'FSCH','A','A_A_','Day of Arrival');
INSERT INTO COLTAB VALUES(407,'FSCH','Na','A_Na_','Flight Nature');
INSERT INTO COLTAB VALUES(408,'FSCH','K','A_K_','Key Code');
INSERT INTO COLTAB VALUES(409,'FSCH','S','A_S_','Service Type');
INSERT INTO COLTAB VALUES(410,'FSCH','ORG','A_ORG_','Origin Airport');
INSERT INTO COLTAB VALUES(411,'FSCH','VIA','A_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(412,'FSCH','POS','A_POS_','Arrival Position');
INSERT INTO COLTAB VALUES(413,'FSCH','Gate1','A_Gate1_','Arrival Gate 1');
INSERT INTO COLTAB VALUES(414,'FSCH','Gate2','A_Gate2_','Arrival Gate 2');
INSERT INTO COLTAB VALUES(415,'FSCH','Belt','A_Belt_','Baggage Belt');
INSERT INTO COLTAB VALUES(416,'FSCH','I','A_I_','?');
INSERT INTO COLTAB VALUES(417,'FSCH','Type','A_Type_','Aircraft Type');
INSERT INTO COLTAB VALUES(418,'FSCH','Registration','A_Registration_','Aircraft Registration');
INSERT INTO COLTAB VALUES(419,'FSCH','Departure','D_Departure_','Departure Flight Number');
INSERT INTO COLTAB VALUES(420,'FSCH','ID','D_ID_','Inter/Domestic');
INSERT INTO COLTAB VALUES(421,'FSCH','J','D_J_','Number of Codeshared Flight');
INSERT INTO COLTAB VALUES(422,'FSCH','Date','D_Date_','Schedule Off-Block Date');
INSERT INTO COLTAB VALUES(423,'FSCH','SOBT','D_SOBT_','Schedule Off-Block Time');
INSERT INTO COLTAB VALUES(424,'FSCH','A','D_A_','Day of Departure');
INSERT INTO COLTAB VALUES(425,'FSCH','Na','D_Na_','Flight Nature');
INSERT INTO COLTAB VALUES(426,'FSCH','K','D_K_','Key Code');
INSERT INTO COLTAB VALUES(427,'FSCH','S','D_S_','Service Type');
INSERT INTO COLTAB VALUES(428,'FSCH','VIA','D_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(429,'FSCH','DES','D_DES_','Destination Airport');
INSERT INTO COLTAB VALUES(430,'FSCH','POS','D_POS_','Departure Position');
INSERT INTO COLTAB VALUES(431,'FSCH','Gate1','D_Gate1_','Departure Gate 1');
INSERT INTO COLTAB VALUES(432,'FSCH','Gate2','D_Gate2_','Departure Gate 2');
INSERT INTO COLTAB VALUES(433,'FSCH','Cki','D_Cki_','Check-In Counter');
INSERT INTO COLTAB VALUES(434,'FSCH','I','D_I_','?');
INSERT INTO COLTAB VALUES(435,'FSCH','Rem. ARR Seasonal','A_Rem. ARR Seasonal_','Arrival Remark for Seasonal Schedule');
INSERT INTO COLTAB VALUES(436,'FSCH','Rem. 1 ARR Daily','A_Rem. 1 ARR Daily_','Arrival Remark 1 for Daily Schedule');
INSERT INTO COLTAB VALUES(437,'FSCH','Rem. DEP Seasonal','D_Rem. DEP Seasonal_','Departure Remark for Seasonal Schedule');
INSERT INTO COLTAB VALUES(438,'FSCH','Rem. 1 DEP Daily','D_Rem. 1 DEP Daily_','Departure Remark 1 for Daily Schedule');
COMMIT;

INSERT INTO SCNTAB VALUES('DARO','Daily Rotation');
COMMIT;

INSERT INTO COLTAB VALUES(501,'DARO','FLNR','A_FLNR_','Arrival Flight Number');
INSERT INTO COLTAB VALUES(502,'DARO','DT','A_DT_','Arrival Date');
INSERT INTO COLTAB VALUES(503,'DARO','ID','A_ID_','Inter/Domestic');
INSERT INTO COLTAB VALUES(504,'DARO','NA','A_NA_','Flight Nature');
INSERT INTO COLTAB VALUES(505,'DARO','ORG','A_ORG_','Origin Airport');
INSERT INTO COLTAB VALUES(506,'DARO','K','A_K_','Key Code');
INSERT INTO COLTAB VALUES(507,'DARO','VIA','A_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(508,'DARO','ATOT','A_ATOT_','Actual Take-Off Time');
INSERT INTO COLTAB VALUES(509,'DARO','SIBT','A_SIBT_','Schedule In-Block Time');
INSERT INTO COLTAB VALUES(510,'DARO','EIBT','A_EIBT_','Estimated In-Block Time');
INSERT INTO COLTAB VALUES(511,'DARO','TMO','A_TMO_','Twenty Mile');
INSERT INTO COLTAB VALUES(512,'DARO','ALDT/ON-BLOCK','A_ALDT/ON-BLOCK_','Actual Landed Time/In-Block Time');
INSERT INTO COLTAB VALUES(513,'DARO','POS A/D','A_POS A/D_','Arrival Position');
INSERT INTO COLTAB VALUES(514,'DARO','A/C','A_A/C_','Aircraft Type');
INSERT INTO COLTAB VALUES(515,'DARO','REG','A_REG_','Aircraft Registration');
INSERT INTO COLTAB VALUES(516,'DARO','FLNR','D_FLNR_','Departure Flight Number');
INSERT INTO COLTAB VALUES(517,'DARO','DT','D_DT_','Departure Date');
INSERT INTO COLTAB VALUES(518,'DARO','ID','D_ID_','Inter/Domestic');
INSERT INTO COLTAB VALUES(519,'DARO','NA','D_NA_','Flight Nature');
INSERT INTO COLTAB VALUES(520,'DARO','VIA','D_VIA_','Via Airport');
INSERT INTO COLTAB VALUES(521,'DARO','DES','D_DES_','Destination Airport');
INSERT INTO COLTAB VALUES(522,'DARO','K','D_K_','Key Code');
INSERT INTO COLTAB VALUES(523,'DARO','SOBT','D_SOBT_','Schedule Off-Block Time');
INSERT INTO COLTAB VALUES(524,'DARO','EOBT','D_EOBT_','Estimated Off-Block Time');
INSERT INTO COLTAB VALUES(525,'DARO','AOBT/ATOT','D_AOBT/ATOT_','Actual Off-Blick Time/Actual Take-Off Time');
INSERT INTO COLTAB VALUES(526,'DARO','CKI','D_CKI_','Check-In Counter');
INSERT INTO COLTAB VALUES(527,'DARO','GATE','D_GATE_','Departure Gate');
COMMIT;
