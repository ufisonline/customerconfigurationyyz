
DELETE SYS_DOMAIN WHERE ID = '2';
-- INSERT BlockedResource record to SYS_DOMAIN table
Insert into SYS_DOMAIN (ID,NAME,DESCR,MAIN_TABLE,AVAIL_RESC,REC_STATUS,ID_HOPO,ID_SCENARIO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('2','BlockedResource',' ','BLKTAB','*',' ','YYZ',' ',null,to_date('01/09/14','DD/MM/RR'),null,null,null,0);


DELETE SYS_DOMAINATTRIBUTE WHERE ID IN ('51','52','53','54','55','56');
-- INSERT BlockedResource ATTRIBUTES to SYS_DOMAINATTRIBUTE table
Insert into SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('51','2','Id',3,'BLKTAB','URNO',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);
Insert into SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('52','2','ResourceName',3,'BLKTAB','BURN',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);
Insert into SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('53','2','FromDateTime',2,'BLKTAB','NAFR',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);
Insert into SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('54','2','ToDateTime',2,'BLKTAB','NATO',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);
Insert into SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('55','2','Resn',3,'BLKTAB','RESN',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);
Insert into SYS_DOMAINATTRIBUTE (ID,ID_SYS_DATADOMAIN,NAME,DATA_TYPE,TABLE_NAME,COL_NAME,DESCR,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('56','2','ResourceCategory',3,'BLKTAB','TABN',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);

DELETE RULE_DETAIL WHERE ID = '5609981e-aea2-499e-964a-7859fc63b855';
-- INSERT BLOCKING_RULE to RULE_DETAIL
Insert into RULE_DETAIL (ID,NAME,DESCR,RES_TYPE,RULE_TYPE,DEF_SCORE,MSG,CLIENT_RULE_CRITERIA1,CLIENT_RULE_CRITERIA2,SERVER_RULE_CRITERIA1,SERVER_RULE_CRITERIA2,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('5609981e-aea2-499e-964a-7859fc63b855','BLOCKING_RULE',' ','*',-1,-999,'Resource has conflicts for blocking rule.','{"RuleDomainObjects":[{"ItemId":"2""Order":1}{"ItemId":"1""Order":1}]"RuleFunctionObjects":[{"ItemId":"8""Order":1"ParameterValues":["$BlockedResource1.ResourceCategory""$BlockedResource1.ResourceCategory"]}{"ItemId":"8""Order":2"ParameterValues":["$BlockedResource1.ResourceCategory""$BlockedResource1.ResourceCategory"]}{"ItemId":"5""Order":1"ParameterValues":["$ResourceAlloc1.FromDateTime""$ResourceAlloc1.ToDateTime""$BlockedResource1.Nafr""$BlockedResource1.Nato"]}]"RuleCriteriaString":"[$$isEquals1] = ''true'' And [$$isEquals2] = ''true'' And [$$findGapInMinutes1] < ''0''"}',' ','$BlockedResource1:BlockedResource()  
$ResourceAlloc1:ResourceAlloc((isEquals($BlockedResource1.ResourceCategory $ResourceAlloc1.ResourceCategory) == ''true'') &&(isEquals($BlockedResource1.ResourceName $ResourceAlloc1.ResourceName) == ''true'') &&(isOverlap($ResourceAlloc1.FromDateTime $ResourceAlloc1.ToDateTime $BlockedResource1.FromDateTime $BlockedResource1.ToDateTime 0.00) == ''true''))',' ',' ','YYZ',null,to_date('04/09/14','DD/MM/RR'),to_date('04/09/14','DD/MM/RR'),'UFIS$ADMIN','UFIS$ADMIN',0);



COMMIT;