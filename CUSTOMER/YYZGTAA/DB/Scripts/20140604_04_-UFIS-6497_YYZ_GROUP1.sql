CREATE TABLE	Current_Arrivals	( 
ROW_CREATE_TIME DATE  DEFAULT NULL ,
ROW_DATE_TIME DATE  DEFAULT NULL ,
CUSER VARCHAR2 ( 30 ) DEFAULT ' ' ,
MUSER VARCHAR2 ( 30 ) DEFAULT ' ' ,
APPN_ROW_ID NUMBER ( 22 ) DEFAULT NULL ,
MTERM VARCHAR2 ( 10 ) DEFAULT NULL ,
SOURCE VARCHAR2 ( 8 ) DEFAULT ' ' ,
TO_BE_ARCHIVED VARCHAR2 ( 1 ) DEFAULT NULL ,
AID NUMBER ( 22 ) DEFAULT NULL ,
AOD VARCHAR2 ( 3 ) DEFAULT ' ' ,
FLC VARCHAR2 ( 3 ) DEFAULT ' ' ,
FLN VARCHAR2 ( 4 ) DEFAULT ' ' ,
FLX VARCHAR2 ( 1 ) DEFAULT ' ' ,
FLT VARCHAR2 ( 8 ) DEFAULT ' ' ,
FLS VARCHAR2 ( 3 ) DEFAULT ' ' ,
FLK VARCHAR2 ( 20 ) DEFAULT ' ' ,
SDT VARCHAR2 ( 8 ) DEFAULT ' ' ,
DAY VARCHAR2 ( 1 ) DEFAULT ' ' ,
DOP VARCHAR2 ( 2 ) DEFAULT ' ' ,
PLC VARCHAR2 ( 3 ) DEFAULT ' ' ,
PLN VARCHAR2 ( 4 ) DEFAULT ' ' ,
PLX VARCHAR2 ( 1 ) DEFAULT ' ' ,
PLT VARCHAR2 ( 8 ) DEFAULT ' ' ,
PCN VARCHAR2 ( 30 ) DEFAULT ' ' ,
LGO VARCHAR2 ( 3 ) DEFAULT NULL ,
ALN VARCHAR2 ( 3 ) DEFAULT ' ' ,
LKC VARCHAR2 ( 3 ) DEFAULT ' ' ,
LKN VARCHAR2 ( 4 ) DEFAULT ' ' ,
LKX VARCHAR2 ( 1 ) DEFAULT ' ' ,
LKF VARCHAR2 ( 8 ) DEFAULT ' ' ,
LKD VARCHAR2 ( 1 ) DEFAULT ' ' ,
LKS VARCHAR2 ( 8 ) DEFAULT ' ' ,
SST VARCHAR2 ( 2 ) DEFAULT ' ' ,
MFC VARCHAR2 ( 3 ) DEFAULT ' ' ,
MFN VARCHAR2 ( 4 ) DEFAULT ' ' ,
MFX VARCHAR2 ( 1 ) DEFAULT ' ' ,
MFF VARCHAR2 ( 8 ) DEFAULT ' ' ,
ORG VARCHAR2 ( 3 ) DEFAULT ' ' ,
VI1 VARCHAR2 ( 3 ) DEFAULT ' ' ,
VI2 VARCHAR2 ( 3 ) DEFAULT ' ' ,
VI3 VARCHAR2 ( 3 ) DEFAULT ' ' ,
LOD VARCHAR2 ( 1 ) DEFAULT NULL ,
FSC VARCHAR2 ( 1 ) DEFAULT ' ' ,
NAT VARCHAR2 ( 3 ) DEFAULT ' ' ,
FST VARCHAR2 ( 1 ) DEFAULT ' ' ,
TRA VARCHAR2 ( 1 ) DEFAULT NULL ,
HDL VARCHAR2 ( 3 ) DEFAULT NULL ,
HPX VARCHAR2 ( 3 ) DEFAULT NULL ,
HRA VARCHAR2 ( 3 ) DEFAULT NULL ,
HCG VARCHAR2 ( 3 ) DEFAULT NULL ,
HCA VARCHAR2 ( 3 ) DEFAULT NULL ,
HMM VARCHAR2 ( 3 ) DEFAULT NULL ,
HBG VARCHAR2 ( 3 ) DEFAULT NULL ,
HRF VARCHAR2 ( 3 ) DEFAULT NULL ,
HBS VARCHAR2 ( 3 ) DEFAULT NULL ,
"REM" VARCHAR2 ( 3 ) DEFAULT ' ' ,
PAX VARCHAR2 ( 3 ) DEFAULT ' ' ,
PXS VARCHAR2 ( 1 ) DEFAULT ' ' ,
CRW VARCHAR2 ( 3 ) DEFAULT NULL ,
CGA VARCHAR2 ( 6 ) DEFAULT NULL ,
MLA VARCHAR2 ( 5 ) DEFAULT NULL ,
BAG VARCHAR2 ( 5 ) DEFAULT NULL ,
RM1 VARCHAR2 ( 22 ) DEFAULT NULL ,
RM2 VARCHAR2 ( 22 ) DEFAULT NULL ,
RM3 VARCHAR2 ( 22 ) DEFAULT NULL ,
CAN VARCHAR2 ( 3 ) DEFAULT NULL ,
SIT VARCHAR2 ( 64 ) DEFAULT NULL ,
EVE VARCHAR2 ( 100 ) DEFAULT NULL ,
DOC VARCHAR2 ( 1 ) DEFAULT NULL ,
DID NUMBER ( 22 ) DEFAULT NULL ,
STA VARCHAR2 ( 4 ) DEFAULT ' ' ,
EET VARCHAR2 ( 4 ) DEFAULT NULL ,
STP VARCHAR2 ( 4 ) DEFAULT NULL ,
SPD VARCHAR2 ( 8 ) DEFAULT NULL ,
NIP VARCHAR2 ( 4 ) DEFAULT NULL ,
NID VARCHAR2 ( 8 ) DEFAULT NULL ,
EDP VARCHAR2 ( 4 ) DEFAULT NULL ,
DEP VARCHAR2 ( 8 ) DEFAULT NULL ,
ABP VARCHAR2 ( 4 ) DEFAULT NULL ,
ABD VARCHAR2 ( 8 ) DEFAULT NULL ,
ETA VARCHAR2 ( 4 ) DEFAULT ' ' ,
EDT VARCHAR2 ( 8 ) DEFAULT ' ' ,
LTA VARCHAR2 ( 4 ) DEFAULT ' ' ,
LAX VARCHAR2 ( 1 ) DEFAULT ' ' ,
LTX VARCHAR2 ( 5 ) DEFAULT ' ' ,
LDA VARCHAR2 ( 8 ) DEFAULT ' ' ,
TDT VARCHAR2 ( 4 ) DEFAULT ' ' ,
TDD VARCHAR2 ( 8 ) DEFAULT ' ' ,
ATA VARCHAR2 ( 4 ) DEFAULT ' ' ,
ADT VARCHAR2 ( 8 ) DEFAULT ' ' ,
IR1 VARCHAR2 ( 2 ) DEFAULT NULL ,
IR2 VARCHAR2 ( 2 ) DEFAULT NULL ,
IR3 VARCHAR2 ( 2 ) DEFAULT NULL ,
DL1 VARCHAR2 ( 4 ) DEFAULT NULL ,
DL2 VARCHAR2 ( 4 ) DEFAULT NULL ,
DL3 VARCHAR2 ( 4 ) DEFAULT NULL ,
TWT VARCHAR2 ( 4 ) DEFAULT NULL ,
TWD VARCHAR2 ( 8 ) DEFAULT NULL ,
RHA VARCHAR2 ( 1 ) DEFAULT NULL ,
TER VARCHAR2 ( 2 ) DEFAULT ' ' ,
LDS VARCHAR2 ( 1 ) DEFAULT NULL ,
PIR VARCHAR2 ( 5 ) DEFAULT ' ' ,
CA1 VARCHAR2 ( 5 ) DEFAULT ' ' ,
CA2 VARCHAR2 ( 5 ) DEFAULT ' ' ,
FBT VARCHAR2 ( 4 ) DEFAULT ' ' ,
FBD VARCHAR2 ( 8 ) DEFAULT ' ' ,
LBT VARCHAR2 ( 4 ) DEFAULT ' ' ,
LBD VARCHAR2 ( 8 ) DEFAULT ' ' ,
GAT VARCHAR2 ( 5 ) DEFAULT ' ' ,
ROS VARCHAR2 ( 1 ) DEFAULT ' ' ,
ROP VARCHAR2 ( 1 ) DEFAULT ' ' ,
ROB VARCHAR2 ( 1 ) DEFAULT ' ' ,
REG VARCHAR2 ( 12 ) DEFAULT ' ' ,
RGA VARCHAR2 ( 3 ) DEFAULT NULL ,
FIN VARCHAR2 ( 4 ) DEFAULT ' ' ,
ACM VARCHAR2 ( 3 ) DEFAULT NULL ,
CAP VARCHAR2 ( 3 ) DEFAULT ' ' ,
BDG VARCHAR2 ( 1 ) DEFAULT NULL ,
TAR VARCHAR2 ( 5 ) DEFAULT NULL ,
RWY VARCHAR2 ( 4 ) DEFAULT ' ' ,
TYP VARCHAR2 ( 3 ) DEFAULT ' ' ,
TYS VARCHAR2 ( 3 ) DEFAULT ' ' ,
RTA VARCHAR2 ( 4 ) DEFAULT NULL ,
ITA VARCHAR2 ( 4 ) DEFAULT NULL ,
OTA VARCHAR2 ( 4 ) DEFAULT NULL ,
GTA VARCHAR2 ( 4 ) DEFAULT ' ' ,
RAD VARCHAR2 ( 8 ) DEFAULT NULL ,
IAD VARCHAR2 ( 8 ) DEFAULT NULL ,
OAD VARCHAR2 ( 8 ) DEFAULT NULL ,
GAD VARCHAR2 ( 8 ) DEFAULT ' ' ,
WEIGHT VARCHAR2 ( 8 ) DEFAULT NULL ,
CHP NUMBER ( 22 ) DEFAULT NULL ,
SFC VARCHAR2 ( 3 ) DEFAULT NULL ,
TAG NUMBER ( 2 ) DEFAULT NULL ,
MIS NUMBER ( 2 ) DEFAULT NULL ,
CSF VARCHAR2 ( 49 ) DEFAULT ' ' ,
LSF VARCHAR2 ( 49 ) DEFAULT ' ' ,
ASF VARCHAR2 ( 49 ) DEFAULT ' ' ,
ZON VARCHAR2 ( 3 ) DEFAULT ' ' ,
SHP VARCHAR2 ( 4 ) DEFAULT NULL ,
CTY VARCHAR2 ( 18 ) DEFAULT ' ' ,
CT1 VARCHAR2 ( 18 ) DEFAULT ' ' ,
CT2 VARCHAR2 ( 18 ) DEFAULT ' ' ,
CT3 VARCHAR2 ( 18 ) DEFAULT ' ' ,
SYM VARCHAR2 ( 3 ) DEFAULT NULL ,
ROT VARCHAR2 ( 1 ) DEFAULT ' ' ,
MPC VARCHAR2 ( 3 ) DEFAULT ' ' ,
MPN VARCHAR2 ( 4 ) DEFAULT ' ' ,
MPX VARCHAR2 ( 1 ) DEFAULT ' ' ,
BCD VARCHAR2 ( 8 ) DEFAULT NULL ,
BCT VARCHAR2 ( 4 ) DEFAULT NULL ,
GCD VARCHAR2 ( 8 ) DEFAULT NULL ,
GCT VARCHAR2 ( 4 ) DEFAULT NULL ,
BLC VARCHAR2 ( 3 ) DEFAULT ' ' ,
BSF VARCHAR2 ( 49 ) DEFAULT NULL ,
MBC VARCHAR2 ( 3 ) DEFAULT NULL 
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
  )
  TABLESPACE "CEDA01" ;


COMMENT ON COLUMN Current_Arrivals.ROW_CREATE_TIME IS 'flight Create time ';
COMMENT ON COLUMN Current_Arrivals.ROW_DATE_TIME IS 'Flight Updated time';
COMMENT ON COLUMN Current_Arrivals.CUSER IS 'create user';
COMMENT ON COLUMN Current_Arrivals.MUSER IS 'update user';
COMMENT ON COLUMN Current_Arrivals.APPN_ROW_ID IS 'ultra custom control ID - not used for the interfaces';
COMMENT ON COLUMN Current_Arrivals.MTERM IS 'ultra custom control ID - not used for the interfaces';
COMMENT ON COLUMN Current_Arrivals.SOURCE IS 'Incoming / outigoing source like SAC - SITA etc';
COMMENT ON COLUMN Current_Arrivals.TO_BE_ARCHIVED IS 'ultra custom control ID - not used for the interfaces';
COMMENT ON COLUMN Current_Arrivals.AID IS 'arrival ID';
COMMENT ON COLUMN Current_Arrivals.AOD IS 'ARR or DEP or A / D';
COMMENT ON COLUMN Current_Arrivals.FLC IS 'Airline ICAO - ALC3';
COMMENT ON COLUMN Current_Arrivals.FLN IS 'Flight Number formatted 	';
COMMENT ON COLUMN Current_Arrivals.FLX IS 'Suffix';
COMMENT ON COLUMN Current_Arrivals.FLT IS 'ALC3/ALC2 + FLNO';
COMMENT ON COLUMN Current_Arrivals.FLS IS 'ALC2	: IATA airline code';
COMMENT ON COLUMN Current_Arrivals.FLK IS 'ARIS PK ';
COMMENT ON COLUMN Current_Arrivals.SDT IS 'Schedule Day YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.DAY IS 'Week Day  - Monday is 1';
COMMENT ON COLUMN Current_Arrivals.DOP IS 'Day of Operation - Flights that are until 04:00  will have a DOP of Yesterday: From the date (day of the date)';
COMMENT ON COLUMN Current_Arrivals.PLC IS 'Carrier code that will be shown to the FIDS might be different from the Main Carrier	: IATA code';
COMMENT ON COLUMN Current_Arrivals.PLN IS 'Flight Number  that will be shown to the FIDS might be different from the Main FLNO';
COMMENT ON COLUMN Current_Arrivals.PLX IS 'Suffix that will be shown to the FIDS might be different from the Main Suffix ??';
COMMENT ON COLUMN Current_Arrivals.PLT IS 'ALC2/ALC3 + FLNO that will be shown to the FIDS might be different from the FLNO';
COMMENT ON COLUMN Current_Arrivals.PCN IS 'Carrier Name that will be shown to the FIDS might be different from the Main Carrier	';
COMMENT ON COLUMN Current_Arrivals.LGO IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.ALN IS 'The Arrival Carrier link with the Airline Table (ALC3)  	: ICAO Code ';
COMMENT ON COLUMN Current_Arrivals.LKC IS ' The Departure Carrier link with the Airline Table (ALC3)	   : ICAO Code ';
COMMENT ON COLUMN Current_Arrivals.LKN IS 'The Flight Number Link with the Departure';
COMMENT ON COLUMN Current_Arrivals.LKX IS 'The Suffix Link with the Departure';
COMMENT ON COLUMN Current_Arrivals.LKF IS 'The Formatted (ALC2/ALC3+ FLNO) for the Departure';
COMMENT ON COLUMN Current_Arrivals.LKD IS 'The  Week Day - Monday is 1 for the Departure';
COMMENT ON COLUMN Current_Arrivals.LKS IS 'Schedule Day YYYYMMDD for the Departure';
COMMENT ON COLUMN Current_Arrivals.SST IS ' Code Share ';
COMMENT ON COLUMN Current_Arrivals.MFC IS 'Master Carrier Code ALC3	: Operating ICAO Code';
COMMENT ON COLUMN Current_Arrivals.MFN IS 'Master Flight Number 	 : Operating';
COMMENT ON COLUMN Current_Arrivals.MFX IS 'Master Suffix	 : Operating';
COMMENT ON COLUMN Current_Arrivals.MFF IS 'Master ALC2/ALC3+FLNO';
COMMENT ON COLUMN Current_Arrivals.ORG IS 'Origin';
COMMENT ON COLUMN Current_Arrivals.VI1 IS 'VIA 1  (ALC3)';
COMMENT ON COLUMN Current_Arrivals.VI2 IS 'VIA 2';
COMMENT ON COLUMN Current_Arrivals.VI3 IS 'VIA 3';
COMMENT ON COLUMN Current_Arrivals.LOD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.FSC IS 'Flight type I - D  T - P';
COMMENT ON COLUMN Current_Arrivals.NAT IS 'Flight Nature	: PAX (passenger)or FE Ror CHT (Charter)';
COMMENT ON COLUMN Current_Arrivals.FST IS 'Service Type';
COMMENT ON COLUMN Current_Arrivals.TRA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.HDL IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HPX IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HRA IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HCG IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HCA IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HMM IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HBG IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HRF IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.HBS IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.REM IS 'Remark - ARR / DEP / DEL/CAN/ERL';
COMMENT ON COLUMN Current_Arrivals.PAX IS 'Pax Total';
COMMENT ON COLUMN Current_Arrivals.PXS IS 'Pax from the MVT (to be confirmed)';
COMMENT ON COLUMN Current_Arrivals.CRW IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.CGA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.MLA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.BAG IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.RM1 IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.RM2 IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.RM3 IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.CAN IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.SIT IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.EVE IS 'Not used (to be confirmed) - filled when Arrived / Landed';
COMMENT ON COLUMN Current_Arrivals.DOC IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.DID IS 'PK from the Departure Flight';
COMMENT ON COLUMN Current_Arrivals.STA IS 'Schedule Time of the Arrival HHMM';
COMMENT ON COLUMN Current_Arrivals.EET IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.STP IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.SPD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.NIP IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.NID IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.EDP IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.DEP IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.ABP IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.ABD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.ETA IS 'Estimated Time of arrival HHMM';
COMMENT ON COLUMN Current_Arrivals.EDT IS 'Estimated Day YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.LTA IS 'Best Time  HHMM';
COMMENT ON COLUMN Current_Arrivals.LAX IS 'S - Schedule - E estimated - A actual  	(associated to best Time)';
COMMENT ON COLUMN Current_Arrivals.LTX IS 'LTA+LAX';
COMMENT ON COLUMN Current_Arrivals.LDA IS 'Best Day YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.TDT IS 'Landed time HHMM';
COMMENT ON COLUMN Current_Arrivals.TDD IS 'Landed Date YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.ATA IS 'OnBlock Time HHMM';
COMMENT ON COLUMN Current_Arrivals.ADT IS 'OnBlock Date YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.IR1 IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.IR2 IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.IR3 IS 'Not u Not used (Delay Code)sed';
COMMENT ON COLUMN Current_Arrivals.DL1 IS 'Not used (Delay Code)';
COMMENT ON COLUMN Current_Arrivals.DL2 IS 'Not used (Delay Code)';
COMMENT ON COLUMN Current_Arrivals.DL3 IS 'Not used (Delay Code)';
COMMENT ON COLUMN Current_Arrivals.TWT IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.TWD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.RHA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.TER IS 'Terminal 1 - 3 /  5( used for parking)';
COMMENT ON COLUMN Current_Arrivals.LDS IS 'Not used	(L value)';
COMMENT ON COLUMN Current_Arrivals.PIR IS 'Pier';
COMMENT ON COLUMN Current_Arrivals.CA1 IS 'Carousel 1 	: Bag Delivery';
COMMENT ON COLUMN Current_Arrivals.CA2 IS 'Carousel 2  : Bag Delivery';
COMMENT ON COLUMN Current_Arrivals.FBT IS 'First bag Time HHMM';
COMMENT ON COLUMN Current_Arrivals.FBD IS 'First Bag date  YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.LBT IS 'Last Bag Time HHMM';
COMMENT ON COLUMN Current_Arrivals.LBD IS 'Last Bag Date YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.GAT IS 'Parking Position';
COMMENT ON COLUMN Current_Arrivals.ROS IS 'Roll out from StaffPages     / A remove';
COMMENT ON COLUMN Current_Arrivals.ROP IS 'Roll out from Public    / A remove';
COMMENT ON COLUMN Current_Arrivals.ROB IS 'Roll out from BIDS   / A remove';
COMMENT ON COLUMN Current_Arrivals.REG IS 'Registration (with - )';
COMMENT ON COLUMN Current_Arrivals.RGA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.FIN IS 'Tail Number';
COMMENT ON COLUMN Current_Arrivals.ACM IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.CAP IS 'NOSE - Number of the seats';
COMMENT ON COLUMN Current_Arrivals.BDG IS 'Not used / Bridge';
COMMENT ON COLUMN Current_Arrivals.TAR IS 'Previous Parking Position - Not used';
COMMENT ON COLUMN Current_Arrivals.RWY IS 'Runway';
COMMENT ON COLUMN Current_Arrivals.TYP IS 'Aircraft code IATA';
COMMENT ON COLUMN Current_Arrivals.TYS IS 'Aircraft code IATA 	.';
COMMENT ON COLUMN Current_Arrivals.RTA IS 'Not used /  To be confirmed (Timing)';
COMMENT ON COLUMN Current_Arrivals.ITA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.OTA IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.GTA IS 'Same as the Schedule Time HHMM';
COMMENT ON COLUMN Current_Arrivals.RAD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.IAD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.OAD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.GAD IS 'Same as the Schedule Date YYYYMMDD';
COMMENT ON COLUMN Current_Arrivals.WEIGHT IS 'Not used - MTOW 	or GTOW';
COMMENT ON COLUMN Current_Arrivals.CHP IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.SFC IS 'Not Used';
COMMENT ON COLUMN Current_Arrivals.TAG IS 'Not Used - 0';
COMMENT ON COLUMN Current_Arrivals.MIS IS 'Not Used - 0';
COMMENT ON COLUMN Current_Arrivals.CSF IS 'Code Share Flights with / - with ALC2 Number';
COMMENT ON COLUMN Current_Arrivals.LSF IS 'Code Share Flights with / - with ALC3 Number';
COMMENT ON COLUMN Current_Arrivals.ASF IS 'Code Share Flights with / - with ALC3 Number';
COMMENT ON COLUMN Current_Arrivals.ZON IS 'Exit Name';
COMMENT ON COLUMN Current_Arrivals.SHP IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.CTY IS 'City Name';
COMMENT ON COLUMN Current_Arrivals.CT1 IS 'City Name for VIA 1';
COMMENT ON COLUMN Current_Arrivals.CT2 IS 'City Name for VIA 2';
COMMENT ON COLUMN Current_Arrivals.CT3 IS 'City Name for VIA 3';
COMMENT ON COLUMN Current_Arrivals.SYM IS 'Not used (FIDS use this one ) to be confirmed how';
COMMENT ON COLUMN Current_Arrivals.ROT IS 'Roll out from';
COMMENT ON COLUMN Current_Arrivals.MPC IS 'Master Public  Carrier Code ALC2: IATA code';
COMMENT ON COLUMN Current_Arrivals.MPN IS 'Master Public  Flight  NUMBER';
COMMENT ON COLUMN Current_Arrivals.MPX IS 'Master Public  Suffix';
COMMENT ON COLUMN Current_Arrivals.BCD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.BCT IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.GCD IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.GCT IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.BLC IS 'Baggage Carrier Airline Code: IATA Code';
COMMENT ON COLUMN Current_Arrivals.BSF IS 'Not used';
COMMENT ON COLUMN Current_Arrivals.MBC IS 'Not used';


commit;

CREATE TABLE Current_Departures	(
ROW_CREATE_TIME DATE DEFAULT NULL,
ROW_DATE_TIME DATE DEFAULT NULL,
CUSER VARCHAR2 ( 30 ) DEFAULT ' ',
MUSER VARCHAR2 ( 30 ) DEFAULT ' ',
APPN_ROW_ID NUMBER ( 22 ) DEFAULT 0,
MTERM VARCHAR2 ( 10 ) DEFAULT ' ',
SOURCE VARCHAR2 ( 8 ) DEFAULT ' ',
TO_BE_ARCHIVED VARCHAR2 ( 1 ) DEFAULT ' ',
DID NUMBER ( 22 ) DEFAULT 0,
AOD VARCHAR2 ( 3 ) DEFAULT ' ',
FLC VARCHAR2 ( 3 ) DEFAULT ' ',
FLN VARCHAR2 ( 4 ) DEFAULT ' ',
FLX VARCHAR2 ( 1 ) DEFAULT ' ',
FLT VARCHAR2 ( 8 ) DEFAULT ' ',
FLS VARCHAR2 ( 3 ) DEFAULT ' ',
FLK VARCHAR2 ( 20 ) DEFAULT ' ',
BLC VARCHAR2 ( 3 ) DEFAULT ' ',
LGO VARCHAR2 ( 3 ) DEFAULT NULL,
ALN VARCHAR2 ( 3 ) DEFAULT ' ',
SDT VARCHAR2 ( 8 ) DEFAULT ' ',
DAY VARCHAR2 ( 1 ) DEFAULT ' ',
DOP VARCHAR2 ( 2 ) DEFAULT ' ',
PLC VARCHAR2 ( 3 ) DEFAULT ' ',
PLN VARCHAR2 ( 4 ) DEFAULT ' ',
PLX VARCHAR2 ( 1 ) DEFAULT ' ',
PLT VARCHAR2 ( 8 ) DEFAULT ' ',
PCN VARCHAR2 ( 30 ) DEFAULT ' ',
LKC VARCHAR2 ( 3 ) DEFAULT ' ',
LKN VARCHAR2 ( 4 ) DEFAULT ' ',
LKX VARCHAR2 ( 1 ) DEFAULT ' ',
LKF VARCHAR2 ( 8 ) DEFAULT ' ',
LKD VARCHAR2 ( 1 ) DEFAULT ' ',
LKS VARCHAR2 ( 8 ) DEFAULT ' ',
SST VARCHAR2 ( 2 ) DEFAULT ' ',
MFC VARCHAR2 ( 3 ) DEFAULT ' ',
MFN VARCHAR2 ( 4 ) DEFAULT ' ',
MFX VARCHAR2 ( 1 ) DEFAULT ' ',
MFF VARCHAR2 ( 8 ) DEFAULT ' ',
DES VARCHAR2 ( 3 ) DEFAULT ' ',
VI1 VARCHAR2 ( 3 ) DEFAULT ' ',
VI2 VARCHAR2 ( 3 ) DEFAULT ' ',
VI3 VARCHAR2 ( 3 ) DEFAULT ' ',
LOD VARCHAR2 ( 1 ) DEFAULT ' ',
FSC VARCHAR2 ( 1 ) DEFAULT ' ',
NAT VARCHAR2 ( 3 ) DEFAULT ' ',
FST VARCHAR2 ( 1 ) DEFAULT ' ',
TRA VARCHAR2 ( 1 ) DEFAULT NULL,
HDL VARCHAR2 ( 3 ) DEFAULT NULL,
HPX VARCHAR2 ( 3 ) DEFAULT NULL,
HRA VARCHAR2 ( 3 ) DEFAULT NULL,
HCG VARCHAR2 ( 3 ) DEFAULT NULL,
HCA VARCHAR2 ( 3 ) DEFAULT NULL,
HMM VARCHAR2 ( 3 ) DEFAULT NULL,
HBG VARCHAR2 ( 3 ) DEFAULT NULL,
HRF VARCHAR2 ( 3 ) DEFAULT NULL,
HBS VARCHAR2 ( 3 ) DEFAULT NULL,
HDI VARCHAR2 ( 3 ) DEFAULT NULL,
REM VARCHAR2 ( 3 ) DEFAULT ' ',
PAX VARCHAR2 ( 3 ) DEFAULT ' ',
PXS VARCHAR2 ( 1 ) DEFAULT ' ',
CRW VARCHAR2 ( 3 ) DEFAULT NULL,
BAG VARCHAR2 ( 5 ) DEFAULT NULL,
CID VARCHAR2 ( 11 ) DEFAULT ' ',
CHS VARCHAR2 ( 1 ) DEFAULT ' ',
CIO VARCHAR2 ( 4 ) DEFAULT ' ',
CIC VARCHAR2 ( 4 ) DEFAULT ' ',
ROC VARCHAR2 ( 1 ) DEFAULT ' ',
BLT VARCHAR2 ( 5 ) DEFAULT ' ',
RM1 VARCHAR2 ( 22 ) DEFAULT NULL,
RM2 VARCHAR2 ( 22 ) DEFAULT NULL,
RM3 VARCHAR2 ( 22 ) DEFAULT NULL,
CAN VARCHAR2 ( 3 ) DEFAULT NULL,
SIT VARCHAR2 ( 64 ) DEFAULT NULL,
EVE VARCHAR2 ( 100 ) DEFAULT NULL,
DOC VARCHAR2 ( 1 ) DEFAULT NULL,
AID NUMBER ( 22 ) DEFAULT 0,
STD VARCHAR2 ( 4 ) DEFAULT ' ',
EET VARCHAR2 ( 4 ) DEFAULT NULL,
ETD VARCHAR2 ( 4 ) DEFAULT ' ',
EDT VARCHAR2 ( 8 ) DEFAULT ' ',
LTD VARCHAR2 ( 4 ) DEFAULT ' ',
LAX VARCHAR2 ( 1 ) DEFAULT ' ',
LTX VARCHAR2 ( 5 ) DEFAULT ' ',
LDD VARCHAR2 ( 8 ) DEFAULT ' ',
ABT VARCHAR2 ( 4 ) DEFAULT ' ',
ABD VARCHAR2 ( 8 ) DEFAULT ' ',
ATD VARCHAR2 ( 4 ) DEFAULT ' ',
ADT VARCHAR2 ( 8 ) DEFAULT ' ',
ASB VARCHAR2 ( 4 ) DEFAULT NULL,
ASE VARCHAR2 ( 4 ) DEFAULT NULL,
ADV VARCHAR2 ( 4 ) DEFAULT NULL,
AVD VARCHAR2 ( 8 ) DEFAULT NULL,
IR1 VARCHAR2 ( 2 ) DEFAULT NULL,
IR2 VARCHAR2 ( 2 ) DEFAULT NULL,
IR3 VARCHAR2 ( 2 ) DEFAULT NULL,
DL1 VARCHAR2 ( 4 ) DEFAULT NULL,
DL2 VARCHAR2 ( 4 ) DEFAULT NULL,
DL3 VARCHAR2 ( 4 ) DEFAULT NULL,
TWT VARCHAR2 ( 4 ) DEFAULT NULL,
TWD VARCHAR2 ( 8 ) DEFAULT NULL,
RHA VARCHAR2 ( 1 ) DEFAULT NULL,
BDS VARCHAR2 ( 1 ) DEFAULT NULL,
PIR VARCHAR2 ( 5 ) DEFAULT ' ',
LNG VARCHAR2 ( 4 ) DEFAULT ' ',
GAT VARCHAR2 ( 5 ) DEFAULT ' ',
GPR VARCHAR2 ( 5 ) DEFAULT ' ',
GST VARCHAR2 ( 1 ) DEFAULT ' ',
ROS VARCHAR2 ( 1 ) DEFAULT ' ',
ROP VARCHAR2 ( 1 ) DEFAULT ' ',
ROG VARCHAR2 ( 1 ) DEFAULT ' ',
REG VARCHAR2 ( 12 ) DEFAULT ' ',
RGA VARCHAR2 ( 3 ) DEFAULT NULL,
FIN VARCHAR2 ( 4 ) DEFAULT ' ',
CAP VARCHAR2 ( 3 ) DEFAULT ' ',
ACM VARCHAR2 ( 3 ) DEFAULT NULL,
BDG VARCHAR2 ( 1 ) DEFAULT NULL,
ICE VARCHAR2 ( 1 ) DEFAULT NULL,
TAR VARCHAR2 ( 5 ) DEFAULT ' ',
TER VARCHAR2 ( 2 ) DEFAULT ' ',
RWY VARCHAR2 ( 4 ) DEFAULT ' ',
AIR VARCHAR2 ( 8 ) DEFAULT NULL,
STR VARCHAR2 ( 1 ) DEFAULT ' ',
TYP VARCHAR2 ( 3 ) DEFAULT ' ',
TYS VARCHAR2 ( 3 ) DEFAULT ' ',
RTD VARCHAR2 ( 4 ) DEFAULT ' ',
ITD VARCHAR2 ( 4 ) DEFAULT NULL,
OTD VARCHAR2 ( 4 ) DEFAULT NULL,
GTD VARCHAR2 ( 4 ) DEFAULT ' ',
RDD VARCHAR2 ( 8 ) DEFAULT ' ',
IDD VARCHAR2 ( 8 ) DEFAULT NULL,
ODD VARCHAR2 ( 8 ) DEFAULT NULL,
GDD VARCHAR2 ( 8 ) DEFAULT ' ',
WEIGHT VARCHAR2 ( 8 ) DEFAULT NULL,
CHP NUMBER ( 22 ) DEFAULT NULL,
SFC VARCHAR2 ( 3 ) DEFAULT NULL,
TAG NUMBER ( 22 ) DEFAULT NULL,
MIS NUMBER ( 22 ) DEFAULT NULL,
CSF VARCHAR2 ( 49 ) DEFAULT ' ',
BSF VARCHAR2 ( 49 ) DEFAULT ' ',
ASF VARCHAR2 ( 49 ) DEFAULT ' ',
LSF VARCHAR2 ( 49 ) DEFAULT ' ',
ZON VARCHAR2 ( 3 ) DEFAULT ' ',
SHP VARCHAR2 ( 4 ) DEFAULT ' ',
CTY VARCHAR2 ( 18 ) DEFAULT ' ',
CT1 VARCHAR2 ( 18 ) DEFAULT ' ',
CT2 VARCHAR2 ( 18 ) DEFAULT ' ',
CT3 VARCHAR2 ( 18 ) DEFAULT ' ',
SYM VARCHAR2 ( 3 ) DEFAULT NULL,
CCD VARCHAR2 ( 8 ) DEFAULT NULL,
COD VARCHAR2 ( 8 ) DEFAULT NULL,
CCR VARCHAR2 ( 1 ) DEFAULT NULL,
CIT VARCHAR2 ( 1 ) DEFAULT NULL,
DOI VARCHAR2 ( 1 ) DEFAULT NULL,
TEP VARCHAR2 ( 2 ) DEFAULT NULL,
RM4 VARCHAR2 ( 40 ) DEFAULT ' ',
RM8 VARCHAR2 ( 40 ) DEFAULT ' ',
MPC VARCHAR2 ( 3 ) DEFAULT ' ',
MPN VARCHAR2 ( 4 ) DEFAULT ' ',
MPX VARCHAR2 ( 1 ) DEFAULT ' ',
MBC VARCHAR2 ( 3 ) DEFAULT ' ',
GCD VARCHAR2 ( 8 ) DEFAULT ' ',
GCT VARCHAR2 ( 4 ) DEFAULT ' '
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
  )
  TABLESPACE "CEDA01" ;


COMMENT ON COLUMN Current_Departures.ROW_CREATE_TIME IS 'Create time';
COMMENT ON COLUMN Current_Departures.ROW_DATE_TIME IS 'Updated time';
COMMENT ON COLUMN Current_Departures.CUSER IS 'create user';
COMMENT ON COLUMN Current_Departures.MUSER IS 'update user';
COMMENT ON COLUMN Current_Departures.APPN_ROW_ID IS 'ultra custom control ID - not used for the interfaces';
COMMENT ON COLUMN Current_Departures.MTERM IS 'ultra custom control ID - not used for the interfaces';
COMMENT ON COLUMN Current_Departures.SOURCE IS 'Incoming / outigoing source like SAC - SITA etc';
COMMENT ON COLUMN Current_Departures.TO_BE_ARCHIVED IS 'ultra custom control ID - not used for the interfaces';
COMMENT ON COLUMN Current_Departures.DID IS 'Departurel ID';
COMMENT ON COLUMN Current_Departures.AOD IS 'ARR or DEP';
COMMENT ON COLUMN Current_Departures.FLC IS 'Airline ICAO - ALC3';
COMMENT ON COLUMN Current_Departures.FLN IS 'Flight Number formatted 	: FLNO';
COMMENT ON COLUMN Current_Departures.FLX IS 'Flight Number formatted 	- Suffix ';
COMMENT ON COLUMN Current_Departures.FLT IS 'ALC3/ALC2 + FLNO';
COMMENT ON COLUMN Current_Departures.FLS IS 'ALC2	- AITA airline code';
COMMENT ON COLUMN Current_Departures.FLK IS 'ARIS PK    - Flight Key	';
COMMENT ON COLUMN Current_Departures.BLC IS 'Baggage Carrier Airline Code';
COMMENT ON COLUMN Current_Departures.LGO IS 'Not used';
COMMENT ON COLUMN Current_Departures.ALN IS 'The Arrival Carrier link with the Airline ';
COMMENT ON COLUMN Current_Departures.SDT IS 'Schedule Day YYYYMMDD';
COMMENT ON COLUMN Current_Departures.DAY IS 'Week Day - Monday is 1	(Days of Operation)';
COMMENT ON COLUMN Current_Departures.DOP IS 'Day of Operation - Flights that are until 04:00  will have a DOP of Yesterday';
COMMENT ON COLUMN Current_Departures.PLC IS 'Carrier that will be shown to the FIDS might be different from the Main Carrier';
COMMENT ON COLUMN Current_Departures.PLN IS 'Flight Number  that will be shown to the FIDS might be different from the Main FLNO';
COMMENT ON COLUMN Current_Departures.PLX IS 'Suffix that will be shown to the FIDS might be different from the Main Suffix ??';
COMMENT ON COLUMN Current_Departures.PLT IS 'ALC2/ALC3 + FLNO that will be shown to the FIDS might be different from the FLNO';
COMMENT ON COLUMN Current_Departures.PCN IS 'Carrier Name that will be shown to the FIDS might be different from the Main Carrier';
COMMENT ON COLUMN Current_Departures.LKC IS 'The Departure Carrier link with the Airline Table (ALC3)';
COMMENT ON COLUMN Current_Departures.LKN IS 'The Flight Number Link with the Arrival';
COMMENT ON COLUMN Current_Departures.LKX IS 'The Suffix Link with the Arrival';
COMMENT ON COLUMN Current_Departures.LKF IS 'The Formatted (ALC2/ALC3+ FLNO) for the Arrival';
COMMENT ON COLUMN Current_Departures.LKD IS 'The  Week Day - Monday is 1 for the Arrival';
COMMENT ON COLUMN Current_Departures.LKS IS 'Schedule Day YYYYMMDD for the Arrival';
COMMENT ON COLUMN Current_Departures.SST IS 'Code Share - If SL is code share - 0 means is the master with no code share - >1 means is master with the number of codeshare';
COMMENT ON COLUMN Current_Departures.MFC IS 'Master Carrier Code ALC3';
COMMENT ON COLUMN Current_Departures.MFN IS 'Master Flight Number';
COMMENT ON COLUMN Current_Departures.MFX IS 'Master Suffix';
COMMENT ON COLUMN Current_Departures.MFF IS 'Master ALC2/ALC3+FLNO';
COMMENT ON COLUMN Current_Departures.DES IS 'Destination ALC3';
COMMENT ON COLUMN Current_Departures.VI1 IS 'VIA 1  (ALC3)';
COMMENT ON COLUMN Current_Departures.VI2 IS 'VIA 2';
COMMENT ON COLUMN Current_Departures.VI3 IS 'VIA 3';
COMMENT ON COLUMN Current_Departures.LOD IS 'Not used';
COMMENT ON COLUMN Current_Departures.FSC IS 'FLIGHT TYPE I - D  T - P';
COMMENT ON COLUMN Current_Departures.NAT IS 'Flight Nature';
COMMENT ON COLUMN Current_Departures.FST IS 'Service Type';
COMMENT ON COLUMN Current_Departures.TRA IS 'Not used';
COMMENT ON COLUMN Current_Departures.HDL IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HPX IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HRA IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HCG IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HCA IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HMM IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HBG IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HRF IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HBS IS 'Not Used';
COMMENT ON COLUMN Current_Departures.HDI IS 'Not Used';
COMMENT ON COLUMN Current_Departures.REM IS 'Remark - ARR / DEP etc';
COMMENT ON COLUMN Current_Departures.PAX IS 'Pax Total';
COMMENT ON COLUMN Current_Departures.PXS IS 'Pax from the MVT (to be confirmed)';
COMMENT ON COLUMN Current_Departures.CRW IS 'Not used';
COMMENT ON COLUMN Current_Departures.BAG IS 'Not used';
COMMENT ON COLUMN Current_Departures.CID IS 'Checkin Counter range with - - eg 001 - 004';
COMMENT ON COLUMN Current_Departures.CHS IS 'Counter Status - C close - O Open';
COMMENT ON COLUMN Current_Departures.CIO IS 'Checkin Open from the earliest open (Schedule/Actual ) HHMM';
COMMENT ON COLUMN Current_Departures.CIC IS 'Checkin Close from the last close  (Schedule/Actual) HHMM';
COMMENT ON COLUMN Current_Departures.ROC IS 'Rolloff for FIDS - A remove to be confirmed where is used';
COMMENT ON COLUMN Current_Departures.BLT IS 'Belt';
COMMENT ON COLUMN Current_Departures.RM1 IS 'Not used';
COMMENT ON COLUMN Current_Departures.RM2 IS 'Not used';
COMMENT ON COLUMN Current_Departures.RM3 IS 'Not used';
COMMENT ON COLUMN Current_Departures.CAN IS 'Not used';
COMMENT ON COLUMN Current_Departures.SIT IS 'Not used';
COMMENT ON COLUMN Current_Departures.EVE IS 'Not used (to be confirmed) - filled when Arrived / Landed';
COMMENT ON COLUMN Current_Departures.DOC IS 'Not used';
COMMENT ON COLUMN Current_Departures.AID IS 'PK from the Arrival Flight';
COMMENT ON COLUMN Current_Departures.STD IS 'Schedule Time of the Departure HHMM';
COMMENT ON COLUMN Current_Departures.EET IS 'Not used';
COMMENT ON COLUMN Current_Departures.ETD IS 'Estimated Time HHMM';
COMMENT ON COLUMN Current_Departures.EDT IS 'Estimated Day YYYYMMDD';
COMMENT ON COLUMN Current_Departures.LTD IS 'Best Time  HHMM';
COMMENT ON COLUMN Current_Departures.LAX IS 'S - Schedule - E estimated - A actual';
COMMENT ON COLUMN Current_Departures.LTX IS 'LTA+LAX';
COMMENT ON COLUMN Current_Departures.LDD IS 'Best Day YYYYMMDD';
COMMENT ON COLUMN Current_Departures.ABT IS 'Airborn Time  HHMM';
COMMENT ON COLUMN Current_Departures.ABD IS 'Airborn Date YYYYMMDD';
COMMENT ON COLUMN Current_Departures.ATD IS 'OffBlock Time HHMM';
COMMENT ON COLUMN Current_Departures.ADT IS 'OffBlock  Date YYYYMMDD';
COMMENT ON COLUMN Current_Departures.ASB IS 'Not used';
COMMENT ON COLUMN Current_Departures.ASE IS 'Not used';
COMMENT ON COLUMN Current_Departures.ADV IS 'Not used';
COMMENT ON COLUMN Current_Departures.AVD IS 'Not used';
COMMENT ON COLUMN Current_Departures.IR1 IS 'Not used';
COMMENT ON COLUMN Current_Departures.IR2 IS 'Not used';
COMMENT ON COLUMN Current_Departures.IR3 IS 'Not used';
COMMENT ON COLUMN Current_Departures.DL1 IS 'Not used (Delay Code)';
COMMENT ON COLUMN Current_Departures.DL2 IS 'Not used (Delay Code)';
COMMENT ON COLUMN Current_Departures.DL3 IS 'Not used (Delay Code)';
COMMENT ON COLUMN Current_Departures.TWT IS 'Not used';
COMMENT ON COLUMN Current_Departures.TWD IS 'Not used';
COMMENT ON COLUMN Current_Departures.RHA IS 'Not used';
COMMENT ON COLUMN Current_Departures.BDS IS 'Not Used';
COMMENT ON COLUMN Current_Departures.PIR IS 'Pier';
COMMENT ON COLUMN Current_Departures.LNG IS 'Gate';
COMMENT ON COLUMN Current_Departures.GAT IS 'Parking Position';
COMMENT ON COLUMN Current_Departures.GPR IS 'Previous Parking Position (Not used)';
COMMENT ON COLUMN Current_Departures.GST IS 'Gate Status - C closed - O Open';
COMMENT ON COLUMN Current_Departures.ROS IS 'Roll out from StaffPages     / A remove';
COMMENT ON COLUMN Current_Departures.ROP IS 'Roll out from Public    / A remove';
COMMENT ON COLUMN Current_Departures.ROG IS 'Roll out from  GATE   - A remove';
COMMENT ON COLUMN Current_Departures.REG IS 'Registration (with - )';
COMMENT ON COLUMN Current_Departures.RGA IS 'Not Used';
COMMENT ON COLUMN Current_Departures.FIN IS 'Tail Number';
COMMENT ON COLUMN Current_Departures.CAP IS 'NOSE - Number of the seats';
COMMENT ON COLUMN Current_Departures.ACM IS 'Not used';
COMMENT ON COLUMN Current_Departures.BDG IS 'Not used / Bridge';
COMMENT ON COLUMN Current_Departures.ICE IS 'Not used (De Ice Y/N)';
COMMENT ON COLUMN Current_Departures.TAR IS 'Previous Parking Position';
COMMENT ON COLUMN Current_Departures.TER IS 'Terminal';
COMMENT ON COLUMN Current_Departures.RWY IS 'Runway';
COMMENT ON COLUMN Current_Departures.AIR IS 'Not used';
COMMENT ON COLUMN Current_Departures.STR IS 'Not used';
COMMENT ON COLUMN Current_Departures.TYP IS 'Aircraft code IATA';
COMMENT ON COLUMN Current_Departures.TYS IS 'Aircraft code IATA';
COMMENT ON COLUMN Current_Departures.RTD IS 'Not used -  To be confirmed';
COMMENT ON COLUMN Current_Departures.ITD IS 'Not used';
COMMENT ON COLUMN Current_Departures.OTD IS 'Not used';
COMMENT ON COLUMN Current_Departures.GTD IS 'Same as the Schedule Time HHMM';
COMMENT ON COLUMN Current_Departures.RDD IS 'Return Date (not used)';
COMMENT ON COLUMN Current_Departures.IDD IS 'Not used';
COMMENT ON COLUMN Current_Departures.ODD IS 'Not used';
COMMENT ON COLUMN Current_Departures.GDD IS '-     Same as the Schedule Date YYYYMMDD';
COMMENT ON COLUMN Current_Departures.WEIGHT IS 'Not used - MTOW 	or GTOW';
COMMENT ON COLUMN Current_Departures.CHP IS 'Not used';
COMMENT ON COLUMN Current_Departures.SFC IS 'Not used';
COMMENT ON COLUMN Current_Departures.TAG IS 'Not Used - 0';
COMMENT ON COLUMN Current_Departures.MIS IS 'Not Used - 0';
COMMENT ON COLUMN Current_Departures.CSF IS 'Code Share Flights separate with / - with ALC2 Number';
COMMENT ON COLUMN Current_Departures.BSF IS 'Code Share Flights separate with / - with ALC2 Number';
COMMENT ON COLUMN Current_Departures.ASF IS 'Code Share Flights separate with / - with ALC3 Number';
COMMENT ON COLUMN Current_Departures.LSF IS 'Code Share Flights separate with / - with ALC3 Number';
COMMENT ON COLUMN Current_Departures.ZON IS ' Pier ';
COMMENT ON COLUMN Current_Departures.SHP IS 'Not used';
COMMENT ON COLUMN Current_Departures.CTY IS 'City Name';
COMMENT ON COLUMN Current_Departures.CT1 IS 'City Name for VIA 1';
COMMENT ON COLUMN Current_Departures.CT2 IS 'City Name for VIA 2';
COMMENT ON COLUMN Current_Departures.CT3 IS 'City Name for VIA 3';
COMMENT ON COLUMN Current_Departures.SYM IS 'Not used (FIDS use this one ) to be confirmed how';
COMMENT ON COLUMN Current_Departures.CCD IS 'Not used';
COMMENT ON COLUMN Current_Departures.COD IS 'Not used';
COMMENT ON COLUMN Current_Departures.CCR IS 'Not used';
COMMENT ON COLUMN Current_Departures.CIT IS 'Not used';
COMMENT ON COLUMN Current_Departures.DOI IS 'Not used';
COMMENT ON COLUMN Current_Departures.TEP IS 'Not used - always 1';
COMMENT ON COLUMN Current_Departures.RM4 IS 'Free Text for Counters - Flight Related?? StaffPages ??';
COMMENT ON COLUMN Current_Departures.RM8 IS 'Free Text for Counters - Flight Related?? StaffPages ??';
COMMENT ON COLUMN Current_Departures.MPC IS 'Master Public  Carrier Code ALC3';
COMMENT ON COLUMN Current_Departures.MPN IS 'Master Public  Flight  Name';
COMMENT ON COLUMN Current_Departures.MPX IS 'Master Public  Suffix';
COMMENT ON COLUMN Current_Departures.MBC IS 'Master Baggage Carrier (Not used ?)';
COMMENT ON COLUMN Current_Departures.GCD IS 'Gate Close Date YYYYMMDD';
COMMENT ON COLUMN Current_Departures.GCT IS 'Gate Close Time HHMM';


COMMIT;

