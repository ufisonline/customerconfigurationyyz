CREATE TABLE "MD_JOURNEY_MATRIX"
  (
    "ID"                 VARCHAR2(40 BYTE) NOT NULL ENABLE,
    "FROM_LOC"           VARCHAR2(64 BYTE) NOT NULL ENABLE,
    "TO_LOC"             VARCHAR2(64 BYTE) NOT NULL ENABLE,
    "FROM_LOC_TYPE"      CHAR(3 BYTE),
    "TO_LOC_TYPE"        CHAR(3 BYTE),
    "OP_TYPE"            CHAR(4 BYTE),
    "DURATION"           NUMBER,
    "MOVE_OBJ_TYPE"      CHAR(3 BYTE),
    "MOVE_OBJ_CATEGORY1" VARCHAR2(64 BYTE),
    "MOVE_OBJ_CATEGORY2" VARCHAR2(64 BYTE),
    "REC_STATUS"         CHAR(1 BYTE) DEFAULT ' ',
    "ID_HOPO"            VARCHAR2(4 BYTE) DEFAULT 'YYZ',
    "DATA_SOURCE"        VARCHAR2(8 BYTE),
    "CREATED_DATE" DATE DEFAULT SYSDATE,
    "UPDATED_DATE" DATE,
    "CREATED_USER" VARCHAR2(32 BYTE),
    "UPDATED_USER" VARCHAR2(32 BYTE),
    "OPT_LOCK"     NUMBER(22,0) DEFAULT 0
  )
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
  )
  TABLESPACE "CEDA02" ;
COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."ID"
IS
  'Unique Record Number - uuid';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."FROM_LOC"
IS
  'From Location';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."TO_LOC"
IS
  'To Location';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."FROM_LOC_TYPE"
IS
  'From Location Type - BLT or BAY or GAT or TER';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."TO_LOC_TYPE"
IS
  'To Location Type - BLT or BAY or GAT or TER';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."OP_TYPE"
IS
  'RAMP or ARRF or DEPF';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."DURATION"
IS
  'Duration of the movement in seconds';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."MOVE_OBJ_TYPE"
IS
  'Movement object type - EQP or PAX';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."MOVE_OBJ_CATEGORY1"
IS
  'Object Name or Type Category 1 i.e. which equip';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."MOVE_OBJ_CATEGORY2"
IS
  'Object Name or Type Category 2 i.e. which equip';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."REC_STATUS"
IS
  'Record status X-delete I-invalid D-disable blank-valid';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."ID_HOPO"
IS
  'Home Airport';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."DATA_SOURCE"
IS
  'Data Source';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."CREATED_DATE"
IS
  'Record creation Date Time - UTC';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."UPDATED_DATE"
IS
  'Record last update Date Time - UTC';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."CREATED_USER"
IS
  'User or process which created this record';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."UPDATED_USER"
IS
  'User or process which modify this record';
  COMMENT ON COLUMN "MD_JOURNEY_MATRIX"."OPT_LOCK"
IS
  'Optimistic Lock';
  CREATE INDEX "MD_JOURNEY_MATRIX_FR_TO_OPT" ON "MD_JOURNEY_MATRIX"
    (
      "FROM_LOC",
      "TO_LOC",
      "OP_TYPE"
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
    )
    TABLESPACE "CEDA02IDX" ;
CREATE UNIQUE INDEX "MD_JOURNEY_MATRIX_ID" ON "MD_JOURNEY_MATRIX"
  (
    "ID"
  )
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
  (
    INITIAL 65536 NEXT 65536 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
  )
  TABLESPACE "CEDA02IDX" ;

delete asztab;
insert into asztab (asiz,wtfr,wtto,urno) values ('I','0000000.00','0000025.00',10015489);
insert into asztab (asiz,wtfr,wtto,urno) values ('II','0000026.00','0000050.00',10015490);
insert into asztab (asiz,wtfr,wtto,urno) values ('III','0000051.00','0000074.00',10015491);
insert into asztab (asiz,wtfr,wtto,urno) values ('IV','0000075.00','0000099.00',10015492);
insert into asztab (asiz,wtfr,wtto,urno) values ('V','0000100.00','0004999.00',10015493);
insert into asztab (asiz,wtfr,wtto,urno) values ('VI','0005000.00','0009999.00',10015494);

commit;


alter table BLTTAB add (
MPAX CHAR(4) default ' ',        
FIDI CHAR(8) default ' ',        
FCHA CHAR(2) default ' ',       
BLTI CHAR(1) default ' ');

DELETE SYSTAB WHERE TANA = 'BLT' and FINA in ('MPAX','FIDI','FCHA','BLTI');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'BLTTAB' 
and a.column_name IN ('MPAX','FIDI','FCHA','BLTI');

commit;
