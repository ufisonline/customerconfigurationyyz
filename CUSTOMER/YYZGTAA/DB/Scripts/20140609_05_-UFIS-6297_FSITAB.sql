CREATE TABLE FSITAB (  
URNO CHAR(10) DEFAULT ' ' ,
SICO CHAR(3) DEFAULT ' ' ,
SINA VARCHAR(32) DEFAULT ' ' ,
USEC CHAR(32) DEFAULT ' ' ,
USEU CHAR(32) DEFAULT ' ' ,
CDAT CHAR(14) DEFAULT ' ' ,
LSTU CHAR(14) DEFAULT ' ' ,
HOPO CHAR(3) DEFAULT ' ' 
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA02" ;    

COMMENT ON COLUMN FSITAB.SICO IS 'SECTOR ID CODE';
COMMENT ON COLUMN FSITAB.SINA IS 'SECTOR ID NAME';

COMMENT ON TABLE FSITAB IS 'Flight Sector ID Table';

CREATE INDEX "FSITAB_SICO" ON "FSITAB"
    (
      "SICO"
    )
    PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE
    (
      INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
    )
    TABLESPACE "CEDA02" ;

DELETE SYSTAB WHERE TANA = 'FSI';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FSITAB' ;

DELETE TABTAB WHERE LTNA = 'FSI';
INSERT INTO TABTAB (LNAM,HOPO,DBTY,LTNA,TATY,LSTU,LTAB,URNO,USEC) 
select replace(nvl(b.comments,'none'),',') "LNAM"
,'YYZ' "HOPO"
,'TAB' "DBTY"
,substr(a.table_name,0,3) LTNA
,substr(a.table_name,0,3) TATY
,TO_CHAR(SYSDATE,'YYYYMMDD') LSTU
,' ' LTAB
,'1' URNO
,'SYSADMIN' USEC
from user_tabLES a, user_tab_comments b 
where a.table_name (+) = b.table_name 
and a.table_name = 'FSITAB' 
;

commit;