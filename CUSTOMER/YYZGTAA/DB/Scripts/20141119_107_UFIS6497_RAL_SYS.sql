alter table raltab add RESS varchar(16) default ' ';
comment on column raltab.RESS is 'Allocation Sub Code. To indicate sub category of allocation.';

DELETE SYSTAB WHERE TANA = 'RAL' and FINA = 'RESS';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'RALTAB' 
and a.column_name = 'RESS';

commit;


alter table sys_domainattribute add allow_group char(1) default ' ';

COMMENT ON COLUMN SYS_DOMAINATTRIBUTE.ALLOW_GROUP IS 'ALLOWED GROUPING';


CREATE TABLE SYS_LOOKUP (  
id VARCHAR(40) DEFAULT ' ' ,
TBL_NAME VARCHAR(30) DEFAULT ' ' ,
COL_NAME VARCHAR(100) DEFAULT ' ' ,
LU_TBL VARCHAR(30) DEFAULT ' ' ,
LU_COL VARCHAR(100) DEFAULT ' ' ,
LU_VAL VARCHAR(4000) DEFAULT ' ' ,
rec_status CHAR(1) DEFAULT ' ' ,
id_hopo VARCHAR2(4) DEFAULT ' ' ,
created_date DATE DEFAULT SYSDATE ,
updated_date DATE DEFAULT Null ,
created_user VARCHAR2(32) DEFAULT Null ,
updated_user VARCHAR2(32) DEFAULT Null ,
opt_lock NUMBER(22,0) DEFAULT 0 
  )    
   PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL  DEFAULT    
  )    
  TABLESPACE "CEDA02" ;    


COMMENT ON COLUMN SYS_LOOKUP.id IS 'ID';
COMMENT ON COLUMN SYS_LOOKUP.TBL_NAME IS 'Table Name';
COMMENT ON COLUMN SYS_LOOKUP.COL_NAME IS 'Column Name';
COMMENT ON COLUMN SYS_LOOKUP.LU_TBL IS 'Lookup Table Name';
COMMENT ON COLUMN SYS_LOOKUP.LU_COL IS 'Lookup Column Name';
COMMENT ON COLUMN SYS_LOOKUP.LU_VAL IS 'Lookup Value (Code and Name pair in JSON format)';
COMMENT ON COLUMN SYS_LOOKUP.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN SYS_LOOKUP.id_hopo IS 'Home Airport';
COMMENT ON COLUMN SYS_LOOKUP.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN SYS_LOOKUP.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN SYS_LOOKUP.created_user IS 'User or process which created this record';
COMMENT ON COLUMN SYS_LOOKUP.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN SYS_LOOKUP.opt_lock IS 'Optimistic Lock';


commit;