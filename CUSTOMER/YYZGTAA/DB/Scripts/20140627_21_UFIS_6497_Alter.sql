alter table sys_datalink add (TABLES_USED VARCHAR(256) DEFAULT ' ');
comment on column sys_datalink.tables_used is 'Tables Used';

ALTER TABLE RULE_ASSIGNMENT ADD (RES_VAR	VARCHAR(100) DEFAULT ' ');
COMMENT ON COLUMN RULE_ASSIGNMENT.RES_VAR IS 'RESOURCE VARIABLE(S)';


commit;