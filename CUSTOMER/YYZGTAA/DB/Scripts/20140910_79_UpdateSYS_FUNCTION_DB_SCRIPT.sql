
-- Update SYS_FUNCTION findGapInMinutes return type to 1.
UPDATE SYS_FUNCTION SET RET_TYPE = '1' WHERE NAME = 'findGapInMinutes';

commit;