alter table swctab modify flti varchar(64);
alter table switab modify flti varchar(64) default ' ';
alter table switab add flto varchar(64) default ' ';

update systab set fele = 64 where tana in ('SWC','SWI') and fina = 'FLTI';

Delete from systab where TANA='SWI';
COMMIT;
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') ADDI
,a.column_name FINA
,'YYZ' HOPO
,a.data_length FELE
, substr(a.data_type,0,1) FITY
, 'N' INDX 
,'TAB' PROJ
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' SYST
,'DATE' TYPE
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'SWITAB' ;
COMMIT;


