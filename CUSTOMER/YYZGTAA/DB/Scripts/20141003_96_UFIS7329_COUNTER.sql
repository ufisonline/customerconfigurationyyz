alter table cictab add ctyp char(3) default ' ';
comment on column cictab.ctyp is 'Counter Type';

DELETE SYSTAB WHERE TANA = 'CIC' AND FINA = 'CTYP';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'CICTAB' 
and a.column_name = 'CTYP';
commit;

CREATE TABLE CRTTAB (  
URNO NUMBER DEFAULT 0 ,
CTYP CHAR(3) DEFAULT ' ' ,
DESR VARCHAR2(128) DEFAULT ' ' ,
ALOC CHAR(3) DEFAULT ' ' ,
HOPO CHAR(3) DEFAULT 'YYZ' ,
CDAT CHAR(14) DEFAULT ' ' ,
LSTU CHAR(14) DEFAULT ' ' ,
USEC VARCHAR2(32) DEFAULT ' ' ,
USEU VARCHAR2(32) DEFAULT ' ' ,
VAFR CHAR(14) DEFAULT ' ' ,
VATO CHAR(14) DEFAULT ' ' ,
PRIMARY KEY (URNO)
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 5242880 NEXT 5242880 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  )    
  TABLESPACE "CEDA02" ;       

COMMENT ON COLUMN CRTTAB.URNO IS 'Unique Record Number';
COMMENT ON COLUMN CRTTAB.CTYP IS 'Counter Type';
COMMENT ON COLUMN CRTTAB.DESR IS 'Counter Type Description';
COMMENT ON COLUMN CRTTAB.ALOC IS 'Allocation Unit - used in demands';
COMMENT ON COLUMN CRTTAB.HOPO IS 'Home Airport';
COMMENT ON COLUMN CRTTAB.CDAT IS 'Created date time';
COMMENT ON COLUMN CRTTAB.LSTU IS 'Updated date time';
COMMENT ON COLUMN CRTTAB.USEC IS 'Created user';
COMMENT ON COLUMN CRTTAB.USEU IS 'Updated user';
COMMENT ON COLUMN CRTTAB.VAFR IS 'Valid from';
COMMENT ON COLUMN CRTTAB.VATO IS 'Valid to';

DELETE SYSTAB WHERE TANA = 'CRT';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'CRTTAB' ;

COMMIT;

delete crttab where urno in (1,2,3,4,5,6);
insert into crttab (URNO,CTYP,DESR,ALOC ) values (1,'CIC','Check in Counter','CIC');
insert into crttab (URNO,CTYP,DESR,ALOC ) values (2,'SEC','Security Counter','SEC');
insert into crttab (URNO,CTYP,DESR,ALOC ) values (3,'IMM','Immigration Counter','IMM');
insert into crttab (URNO,CTYP,DESR,ALOC ) values (4,'TXF','Transfer Counter','TXF');
insert into crttab (URNO,CTYP,DESR,ALOC ) values (5,'LOB','Lost Baggage Counter','LOB');
insert into crttab (URNO,CTYP,DESR,ALOC ) values (6,'CUS','Customs','CUS');

commit; 