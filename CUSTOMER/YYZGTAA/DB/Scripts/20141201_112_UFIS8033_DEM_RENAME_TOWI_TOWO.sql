alter table demtab drop (TOWI,TOWO);
delete systab where tana = 'DEM' and fina in ('TOWI','TOWO');
commit;

alter table demtab add (FCFR char(10) default ' ',FCLR char(10) default ' ');

comment on column demtab.fcfr is 'Flight Chain First Record ';
comment on column demtab.fclr is 'Flight Chain Last Record ';

DELETE SYSTAB WHERE TANA = 'DEM' and FINA in ('FCFR','FCLR');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'DEMTAB' 
and a.column_name IN ('FCFR','FCLR');

COMMIT;

alter table raltab drop (TOWI,TOWO);
delete systab where tana = 'RAL' and fina in ('TOWI','TOWO');
commit;

alter table raltab add (FCFR char(10) default ' ',FCLR char(10) default ' ');

comment on column raltab.fcfr is 'Flight Chain First Record ';
comment on column raltab.fclr is 'Flight Chain Last Record ';

DELETE SYSTAB WHERE TANA = 'RAL' and FINA in ('FCFR','FCLR');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'RALTAB' 
and a.column_name IN ('FCFR','FCLR');

COMMIT;