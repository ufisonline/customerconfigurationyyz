alter table "CFG_MSG_TEMPLATE" add ( "ELEMENT_SEQ" NUMBER  NOT NULL );
alter table "CFG_MSG_TEMPLATE" modify ( "CONTENT_TYPE" VARCHAR2(128) );
alter table "CFG_MSG_TEMPLATE" modify ( "TEXT_FORMAT" VARCHAR2(32) );
alter table "CFG_MSG_TEMPLATE" modify ( "ID" VARCHAR2(40) );

COMMENT ON COLUMN "CFG_MSG_TEMPLATE"."ELEMENT_SEQ" IS 'Message element sequence' ;


  ALTER TABLE "CFG_MSG_TEMPLATE" MODIFY ("ELEMENT_SEQ" NOT NULL ENABLE);
  
  alter table "FSRTAB" add ( "DSRC" VARCHAR2(255) );

COMMENT ON COLUMN "FSRTAB"."DSRC" IS '' ;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'FSRTAB' 
and a.column_name = 'DSRC';

commit;


alter table ODGTAB ADD (
 "CDAT" CHAR(14 BYTE) DEFAULT ' ',
    "LSTU" CHAR(14 BYTE) DEFAULT ' ',
    "USEC" VARCHAR2(32 BYTE) DEFAULT ' ',
    "USEU" VARCHAR2(32 BYTE) DEFAULT ' '
 );

 INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'ODGTAB' 
and a.column_name in ('CDAT','USEC','USEU','LSTU');

commit;