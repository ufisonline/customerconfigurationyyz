CREATE TABLE RULE_ASSIGNMENT_GRP (  
ID VARCHAR(40) DEFAULT ' ' ,
ID_RULE_ASSIGNMENT VARCHAR(40) DEFAULT ' ' ,
GRP_NO NUMBER(3,0) DEFAULT 1 ,
RES_VAR VARCHAR(100) DEFAULT ' ' ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT ' ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT SYSDATE ,
UPDATED_DATE DATE DEFAULT NULL ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0 
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA02" ;    


COMMENT ON COLUMN Rule_Assignment_Grp.id IS 'ID';
COMMENT ON COLUMN Rule_Assignment_Grp.id_rule_assignment IS 'ID of rule Rule_assignment';
COMMENT ON COLUMN Rule_Assignment_Grp.grp_no IS 'Group No.';
COMMENT ON COLUMN Rule_Assignment_Grp.res_var IS 'Resource variable(s) separated by ,';
COMMENT ON COLUMN Rule_Assignment_Grp.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Rule_Assignment_Grp.id_hopo IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Rule_Assignment_Grp.data_source IS 'Home Airport';
COMMENT ON COLUMN Rule_Assignment_Grp.created_date IS 'Data Source';
COMMENT ON COLUMN Rule_Assignment_Grp.updated_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Rule_Assignment_Grp.created_user IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Rule_Assignment_Grp.updated_user IS 'User or process which created this record';
COMMENT ON COLUMN Rule_Assignment_Grp.opt_lock IS 'User or process which modify this record';


commit;

alter table RULE_ASSIGNMENT_DETAIL ADD GRP_NO NUMBER(3,0) default 1;

COMMENT ON COLUMN RULE_ASSIGNMENT_DETAIL.GRP_NO IS 'Group No';

commit;