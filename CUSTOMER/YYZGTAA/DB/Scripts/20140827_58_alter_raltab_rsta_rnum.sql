alter table RALTAB add RNUM CHAR(3 BYTE) default ' ';
alter table RALTAB add RSTA CHAR(4 BYTE) DEFAULT ' ';
alter table RALTAB add FLGS CHAR(10 BYTE) DEFAULT ' ';
commit;

CREATE INDEX "RALTAB_RSTA" ON "RALTAB" ("RSTA") 
PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
STORAGE(INITIAL 1M NEXT 1M MINEXTENTS 1 MAXEXTENTS 2147483645
PCTINCREASE 0 )
TABLESPACE "CEDA01IDX" ;

COMMIT;
COMMENT ON COLUMN RALTAB.RNUM IS 'Resource number';
COMMENT ON COLUMN RALTAB.RSTA IS 'Resource Usage Status';
COMMENT ON COLUMN RALTAB.FLGS IS 'Flags (Demand valid or not or others)';

COMMIT;

DELETE FROM SYSTAB WHERE TANA='RAL';
COMMIT;

INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') ADDI
,a.column_name FINA
,'YYZ' HOPO
,a.data_length FELE
, substr(a.data_type,0,1) FITY
, 'N' INDX 
,'TAB' PROJ
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' SYST
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'RALTAB' 
;

COMMIT;