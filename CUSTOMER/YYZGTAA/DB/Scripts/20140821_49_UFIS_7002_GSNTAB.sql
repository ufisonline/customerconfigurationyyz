DROP TABLE GSNTAB;
CREATE TABLE GSNTAB (  
URNO VARCHAR(10) DEFAULT ' ' ,
GNAM VARCHAR(10) DEFAULT ' ' ,
FLTI VARCHAR(2) DEFAULT ' ' ,
GSNM VARCHAR(32) DEFAULT ' ' ,
CDAT CHAR(14) DEFAULT ' ' ,
HOPO CHAR(3) DEFAULT ' ' ,
LSTU CHAR(14) DEFAULT ' ' ,
USEC CHAR(32) DEFAULT ' ' ,
USEU CHAR(32) DEFAULT ' ' ,
PRIMARY KEY (URNO)
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 5242880 NEXT 5242880 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA02" ;    

COMMENT ON COLUMN GSNTAB.URNO IS 'ID';
COMMENT ON COLUMN GSNTAB.GNAM IS 'Gate Name (GAT.GNAM)';
COMMENT ON COLUMN GSNTAB.FLTI IS 'Sector Id (single sector id)';
COMMENT ON COLUMN GSNTAB.GSNM IS 'Gate Name for Specific Sector';
COMMENT ON COLUMN GSNTAB.CDAT IS 'Record Creation date';
COMMENT ON COLUMN GSNTAB.HOPO IS 'Home Airport';
COMMENT ON COLUMN GSNTAB.LSTU IS 'Last Update';
COMMENT ON COLUMN GSNTAB.USEC IS 'User who created';
COMMENT ON COLUMN GSNTAB.USEU IS 'User who updated';



DELETE SYSTAB WHERE TANA = 'GSN';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'GSNTAB' ;

commit;
