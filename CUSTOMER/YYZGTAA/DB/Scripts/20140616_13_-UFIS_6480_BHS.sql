CREATE TABLE	BHS_TNEW_DEPARTURE_LATERALS	(		
TRANSACTION_TYPE	Varchar2 (1)	DEFAULT	' '	,
SEQ_NR	Number (22)	DEFAULT	0	,
GMTDTS	Date		,
DTS	Date		,
CAN	Varchar2 (3)	DEFAULT	' '	,
ACT	Number (2)	DEFAULT	0	,
FLN	Varchar2 (5)	DEFAULT	' '	,
FLC	Varchar2 (3)	DEFAULT	' '	,
SDT	Date	,
ADT	Date	,
HLD	Number (1)	DEFAULT	0	,
TYP	Varchar2 (3)	DEFAULT	' '	,
DC1	Varchar2 (3)	DEFAULT	' '	,
DC2	Varchar2 (3)	DEFAULT	' '	,
DC3	Varchar2 (3)	DEFAULT	' '	,
DC4	Varchar2 (3)	DEFAULT	' '	,
DC5	Varchar2 (3)	DEFAULT	' '	,
O11	Varchar2 (3)	DEFAULT	' '	,
O12	Varchar2 (3)	DEFAULT	' '	,
O13	Varchar2 (3)	DEFAULT	' '	,
O14	Varchar2 (3)	DEFAULT	' '	,
O15	Varchar2 (3)	DEFAULT	' '	,
O21	Varchar2 (3)	DEFAULT	' '	,
O22	Varchar2 (3)	DEFAULT	' '	,
O23	Varchar2 (3)	DEFAULT	' '	,
O24	Varchar2 (3)	DEFAULT	' '	,
O25	Varchar2 (3)	DEFAULT	' '	,
EBI	Number (4)	DEFAULT	0	,
LBI	Number (4)	DEFAULT	0	,
BLT	Varchar2 (20)	DEFAULT	' '	,
SP1	Varchar2 (20)	DEFAULT	' '	,
SP2	Varchar2 (20)	DEFAULT	' '	,
SP3	Varchar2 (20)	DEFAULT	' '	,
SP4	Varchar2 (20)	DEFAULT	' '	,
SP5	Varchar2 (20)	DEFAULT	' '	,
CS1	Varchar2 (20)	DEFAULT	' '	,
CS2	Varchar2 (20)	DEFAULT	' '	,
CS3	Varchar2 (20)	DEFAULT	' '	,
CS4	Varchar2 (20)	DEFAULT	' '	,
CS5	Varchar2 (20)	DEFAULT	' '	,
OS1	Varchar2 (20)	DEFAULT	' '	,
OS11	Varchar2 (20)	DEFAULT	' '	,
OS12	Varchar2 (20)	DEFAULT	' '	,
OS13	Varchar2 (20)	DEFAULT	' '	,
OS14	Varchar2 (20)	DEFAULT	' '	,
OS15	Varchar2 (20)	DEFAULT	' '	,
OS2	Varchar2 (20)	DEFAULT	' '	,
OS21	Varchar2 (20)	DEFAULT	' '	,
OS22	Varchar2 (20)	DEFAULT	' '	,
OS23	Varchar2 (20)	DEFAULT	' '	,
OS24	Varchar2 (20)	DEFAULT	' '	,
OS25	Varchar2 (20)	DEFAULT	' '	,
ESL	Varchar2 (20)	DEFAULT	' '	,
LSL	Varchar2 (20)	DEFAULT	' '	,
UREF	Number (22)	DEFAULT	0	
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE				
  (				
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT				
  )				
  TABLESPACE "CEDA02" ;				


COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.TRANSACTION_TYPE IS 'The transaction type �I� ';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SEQ_NR IS 'Sequence number ';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.GMTDTS IS 'The download GMT timestamp.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.DTS IS 'The download local timestamp.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.CAN IS 'The cancellation remark.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.ACT IS 'The Action code.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.FLN IS 'The flight number.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.FLC IS 'The carrier code.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SDT IS 'The original (scheduled) flight date/time.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.ADT IS 'The actual flight date/time.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.HLD IS 'Flag to indicate to the BHS if the flight is on hold. It will default to 0.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.TYP IS 'The equipment/aircraft type.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.DC1 IS '1st Destination City (1st port of call).';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.DC2 IS '2nd Destination City (2nd port of call).';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.DC3 IS '3rd Destination City (3rd port of call).';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.DC4 IS '4th Destination City (4th port of call).';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.DC5 IS '5th Destination City. This will be empty.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O11 IS '1st Onward City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O12 IS '2nd Onward City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O13 IS '3rd Onward City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O14 IS '4th Onward City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O15 IS '5th Onward City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O21 IS '1st Onward City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O22 IS '2nd Onward City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O23 IS '3rd Onward City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O24 IS '4th Onward City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.O25 IS '5th Onward City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.EBI IS 'Early Bag Interval. The number of minutes before the actual flight time that the bags will be considered early.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.LBI IS 'Late Bag Interval. The number of minutes before the actual flight time that the flight will be closed out and hence any bags will be considered late.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.BLT IS 'Primary Sort Destination for on-time bags. This will be used if no other sort locations are defined. If this column is also empty then all flight bags for that flight will go to either the default pier for the carrier or if this is not defined to the Manual Encode Station for processing.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SP1 IS 'Special Bag Handling Sort Lateral for 1st Destination City. If it is not an onward flight with an onward sort lateral and this column is defined, then the first class passenger bags for the 1st Destination City will sort to this location.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SP2 IS 'Special Bag Handling Sort Lateral for 2nd Destination City. If it is not an onward flight with an onward sort lateral and this column is defined, then the first class passenger bags for the 2nd Destination City will sort to this location';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SP3 IS 'Special Bag Handling Sort Lateral for 3rd Destination City. If it is not an onward flight with an onward sort lateral and this column is defined, then the first class passenger bags for the 3rd Destination City will sort to this location';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SP4 IS 'Special Bag Handling Sort Lateral for 4th Destination City. If it is not an onward flight with an onward sort lateral and this column is defined, then the first class passenger bags for the 4th Destination City will sort to this location';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.SP5 IS 'Special Bag Handling Sort Lateral for 5th Destination City. If it is not an onward flight with an onward sort lateral and this column is defined, then the first class passenger bags for the 5th Destination City will sort to this location';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.CS1 IS 'City Sort Lateral for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.CS2 IS 'City Sort Lateral for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.CS3 IS 'City Sort Lateral for the 3rd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.CS4 IS 'City Sort Lateral for the 4th Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.CS5 IS 'City Sort Lateral for the 5th Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS1 IS 'Default Onward City Sort Lateral for the 1st Destination City. If no other onward city sort destinations are defined all bags for connecting flights for the 1st Destination City go to this sort location.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS11 IS 'Onward City sort lateral 1 for the 1st Destination City. This column is the sort lateral that corresponds to the 1st Onward Destination City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS12 IS 'Onward City sort lateral 2 for the 1st Destination City. This column is the sort lateral that corresponds to the 2nd Onward Destination City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS13 IS 'Onward City sort lateral 3 for the 1st Destination City. This column is the sort lateral that corresponds to the 3rd Onward Destination City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS14 IS 'Onward City sort lateral 4 for the 1st Destination City. This column is the sort lateral that corresponds to the 4th Onward Destination City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS15 IS 'Onward City sort lateral 1 for the 1st Destination City. This column is the sort lateral that corresponds to the 5th Onward Destination City for the 1st Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS2 IS 'Default Onward City Sort Lateral for the 2nd Destination City. If no other onward city sort destinations are defined all bags for connecting flights for the 2nd Destination City go to this sort location.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS21 IS 'Onward City sort lateral 1 for the 2nd Destination City. This column is the sort lateral that corresponds to the 1st Onward Destination City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS22 IS 'Onward City sort lateral 2 for the 2nd Destination City. This column is the sort lateral that corresponds to the 2nd Onward Destination City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS23 IS 'Onward City sort lateral 3 for the 1st Destination City. This column is the sort lateral that corresponds to the 3rd Onward Destination City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS24 IS 'Onward City sort lateral 4 for the 2nd Destination City. This column is the sort lateral that corresponds to the 4th Onward Destination City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.OS25 IS 'Onward City sort lateral 5 for the 2nd Destination City. This column is the sort lateral that corresponds to the 5th Onward Destination City for the 2nd Destination City';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.ESL IS 'Early Sort Lateral. If this column is defined, all early bags for the flight will be sorted to this location.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.LSL IS 'Late Sort Lateral. If this column is defined, all late bags for the flight will be sorted to this location. If this column is not defined than all late bags will be sorted to the control area-wide late bag location.';
COMMENT ON COLUMN BHS_TNEW_DEPARTURE_LATERALS.UREF IS 'Refer record ID';

COMMIT: