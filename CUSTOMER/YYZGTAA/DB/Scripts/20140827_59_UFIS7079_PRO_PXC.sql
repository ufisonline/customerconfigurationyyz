alter table protab drop (RTDB,RTDE);
alter table protab add (
RTDB VARCHAR(14) DEFAULT ' ',
RTDE VARCHAR(14) DEFAULT ' '
);

COMMENT ON COLUMN PROTAB.RTDB IS '';
COMMENT ON COLUMN PROTAB.RTDE IS '';

DELETE SYSTAB WHERE TANA = 'PRO' AND FINA IN ('RTDB','RTDE');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'PROTAB' 
and a.column_name IN ('RTDB','RTDE');
COMMIT;

alter table PXCTAB add (
QQUA CHAR(4) DEFAULT ' ',
QTYP CHAR(8) DEFAULT ' ',
LDFA CHAR(4) DEFAULT ' '
);

COMMENT ON COLUMN PXCTAB.QQUA is 'Queue Quality - Acceptable number of PAX on queue at one time';
COMMENT ON COLUMN PXCTAB.QTYP is 'Queue Type - SINGLE or COMMON';
COMMENT ON COLUMN PXCTAB.LDFA IS 'Expected load info out of the load info (config) available';

DELETE SYSTAB WHERE TANA = 'PXC' AND FINA IN ('QQUA','QTYP','LDFA');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'PXCTAB' 
and a.column_name IN ('QQUA','QTYP','LDFA');
COMMIT;


alter table PXCTAB modify LDFA CHAR(6);

COMMIT;

update SYSTAB
set fele = 6
where tana = 'PXC' and fina = 'LDFA';

commit;