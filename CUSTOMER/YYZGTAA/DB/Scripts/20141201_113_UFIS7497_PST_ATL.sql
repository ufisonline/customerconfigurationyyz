ALTER TABLE PSTTAB ADD STTY CHAR (1) DEFAULT ' '; 
COMMENT ON COLUMN PSTTAB.STTY IS 'STAND TYPE R-REMOTE,C-CONTACT,H-HELIPAD';

DELETE SYSTAB WHERE TANA = 'PST' and FINA = 'STTY';
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'PSTTAB' 
and a.column_name = 'STTY';

COMMIT;

CREATE TABLE ATLTAB (  
URNO VARCHAR(10) DEFAULT ' ' ,
UALT CHAR(10) DEFAULT ' ' ,
SICO VARCHAR(3) DEFAULT ' ' ,
ADCO CHAR(1) DEFAULT ' ' ,
APC3 CHAR(3) DEFAULT ' ' ,
TERM VARCHAR2(64) DEFAULT ' ' ,
CDAT CHAR(14) DEFAULT ' ' ,
LSTU CHAR(14) DEFAULT ' ' ,
USEC CHAR(32) DEFAULT ' ' ,
USEU CHAR(32) DEFAULT ' ' ,
HOPO CHAR(3) DEFAULT 'YYZ' 
  )    
   PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 1048576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL  DEFAULT    
  )    
  TABLESPACE "CEDA03" ;    

  
COMMENT ON COLUMN ATLTAB.URNO IS 'ID';
COMMENT ON COLUMN ATLTAB.UALT IS 'URNO of ALTTAB (Airline Table)';
COMMENT ON COLUMN ATLTAB.SICO IS 'Sector Code �*� � Any Sector e.g. I = International, D = Domestic';
COMMENT ON COLUMN ATLTAB.ADCO IS 'Arrival/Departure ID ';
COMMENT ON COLUMN ATLTAB.APC3 IS 'Origin/Destination Airport 3 Letters Code';
COMMENT ON COLUMN ATLTAB.TERM IS 'Terminal';
COMMENT ON COLUMN ATLTAB.CDAT IS 'Created Date Time - UTC';
COMMENT ON COLUMN ATLTAB.LSTU IS 'Updated Date Time - UTC';
COMMENT ON COLUMN ATLTAB.USEC IS 'User who created this record';
COMMENT ON COLUMN ATLTAB.USEU IS 'User who updated this record';
COMMENT ON COLUMN ATLTAB.HOPO IS 'Home Airport';


DELETE SYSTAB WHERE TANA = 'ATL' ;
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'ATLTAB' ;

COMMIT;

