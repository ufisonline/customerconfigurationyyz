alter table afttab drop column FINN;
alter table afttab drop column RSTA;
alter table afttab drop column RSTD;
alter table afttab drop column VERS;
alter table afttab drop column ROTA;
alter table afttab add (
FINN VARCHAR(12) default ' ',
RSTA	CHAR(14) default ' ',
RSTD	CHAR(14) default ' ',
VERS	CHAR(40) default ' ',
ROTA	CHAR(1)	default ' ');


comment on column afttab.finn is 'FIN Number';
comment on column afttab.rsta is 'Requested Schedule Time of Arrival';
comment on column afttab.rstd is 'Requested Schedule Time of Departure';
comment on column afttab.vers is 'FSRTAB.URNO';
comment on column afttab.rota is 'Rotate Type';


DELETE SYSTAB WHERE TANA = 'AFT' AND FINA IN ('FINN','RSTA','RSTD','VERS','ROTA');
INSERT INTO SYSTAB (ADDI,FINA,HOPO,FELE,FITY,INDX,PROJ,REFE,REQF,TANA,TATY,SYST,TYPE,LOGD,URNO) 
select replace(nvl(b.comments,'none'),',') "ADDI"
,a.column_name "FINA"
,'YYZ' "HOPO"
,a.data_length "FELE"
, substr(a.data_type,0,1) FITY
, 'N' "INDX" 
,'TAB' "PROJ"
,'.' REFE
,'N' REQF
,substr(a.table_name,0,3) TANA
,substr(a.table_name,0,3) TATY
,'N' "SYST"
, CASE 
   WHEN data_type = 'CHAR' and data_length <> '14' THEN 'TRIM'
   WHEN data_type = 'NUMBER' and data_length <> '14' THEN 'LONG'
   WHEN data_type = 'CHAR' and data_length = '14' THEN 'DATE'
   ELSE 'TRIM'
END 
,' ' LOGD
,'1' URNO
from user_tab_columns a, user_col_comments b 
where a.table_name (+) = b.table_name 
and a.column_name (+) = b.column_name 
and a.table_name = 'AFTTAB' 
and a.column_name IN  ('FINN','RSTA','RSTD','VERS','ROTA');

COMMIT;