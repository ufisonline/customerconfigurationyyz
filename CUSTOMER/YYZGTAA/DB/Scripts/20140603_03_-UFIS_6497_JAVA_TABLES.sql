CREATE TABLE SYS_DOMAIN
  (
id VARCHAR(40) DEFAULT ' ' ,
name VARCHAR(30) DEFAULT ' ' ,
descr VARCHAR(100) DEFAULT ' ' ,
main_table VARCHAR(40) DEFAULT ' ' ,
avail_resc VARCHAR(100) DEFAULT ' ' ,
rec_status CHAR(1) DEFAULT ' ' ,
id_hopo VARCHAR2(4) DEFAULT 'YYZ' ,
id_scenario VARCHAR2(40) DEFAULT ' ' ,
data_source VARCHAR2(8) DEFAULT Null ,
created_date DATE DEFAULT SYSDATE ,
updated_date DATE DEFAULT Null ,
created_user VARCHAR2(32) DEFAULT Null ,
updated_user VARCHAR2(32) DEFAULT Null ,
opt_lock NUMBER(22,0) DEFAULT 0 
  )
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT
  )
  TABLESPACE "CEDA01" ;

COMMENT ON COLUMN SYS_DOMAIN.id IS 'ID';
COMMENT ON COLUMN SYS_DOMAIN.name IS 'Domain Name';
COMMENT ON COLUMN SYS_DOMAIN.descr IS 'Domain Description';
COMMENT ON COLUMN SYS_DOMAIN.main_table IS 'Main Table Name for this Domain';
COMMENT ON COLUMN SYS_DOMAIN.avail_resc IS 'Available for Resource Type Multiple Resource can be defined by using �,� as separator. ALOTAB.ALOC [e.g. PST=Parking Stand, GAT=GATE]';
COMMENT ON COLUMN SYS_DOMAIN.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN SYS_DOMAIN.id_hopo IS 'Home Airport';
COMMENT ON COLUMN SYS_DOMAIN.id_scenario IS 'Scenario ID';
COMMENT ON COLUMN SYS_DOMAIN.data_source IS 'Data Source';
COMMENT ON COLUMN SYS_DOMAIN.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN SYS_DOMAIN.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN SYS_DOMAIN.created_user IS 'User or process which created this record';
COMMENT ON COLUMN SYS_DOMAIN.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN SYS_DOMAIN.opt_lock IS 'Optimistic Lock';



CREATE TABLE SYS_DOMAINATTRIBUTE    (
ID VARCHAR(40) DEFAULT ' ' ,
ID_SYS_DATADOMAIN VARCHAR(40) DEFAULT ' ' ,
NAME VARCHAR(30) DEFAULT ' ' ,
DATA_TYPE NUMBER(1,0) DEFAULT 0 ,
TABLE_NAME VARCHAR(40) DEFAULT ' ' ,
COL_NAME VARCHAR(40) DEFAULT ' ' ,
DESCR VARCHAR(100) DEFAULT ' ' ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT 'YYZ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT SYSDATE ,
UPDATED_DATE DATE DEFAULT NULL ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0 
      )    
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA01" ;   

 COMMENT ON COLUMN Sys_DomainAttribute.id IS 'ID';
COMMENT ON COLUMN Sys_DomainAttribute.id_sys_datadomain IS 'Domain Id (FK: DataDomain.id)';
COMMENT ON COLUMN Sys_DomainAttribute.name IS 'Attribute Name';
COMMENT ON COLUMN Sys_DomainAttribute.data_type IS 'Data Type [1=Number, 2=Date, 3=String]';
COMMENT ON COLUMN Sys_DomainAttribute.table_name IS 'Table Name to take the data ';
COMMENT ON COLUMN Sys_DomainAttribute.col_name IS 'Column Name to take the data';
COMMENT ON COLUMN Sys_DomainAttribute.descr IS 'Description';
COMMENT ON COLUMN Sys_DomainAttribute.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Sys_DomainAttribute.id_hopo IS 'Home Airport';
COMMENT ON COLUMN Sys_DomainAttribute.data_source IS 'Data Source';
COMMENT ON COLUMN Sys_DomainAttribute.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Sys_DomainAttribute.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Sys_DomainAttribute.created_user IS 'User or process which created this record';
COMMENT ON COLUMN Sys_DomainAttribute.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN Sys_DomainAttribute.opt_lock IS 'Optimistic Lock';

 
CREATE TABLE Sys_DataLink    (
ID VARCHAR(40) DEFAULT ' ' ,
RESULT_TYPE NUMBER(1,0) DEFAULT 1 ,
FR_TABLE_NAME VARCHAR(40) DEFAULT ' ' ,
TO_TABLE_NAME VARCHAR(40) DEFAULT ' ' ,
JOIN_CLAUSE VARCHAR(100) DEFAULT ' ' ,
DESCR VARCHAR(100) DEFAULT ' ' ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT 'YYZ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT (SYSDATE) ,
UPDATED_DATE DATE ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0     
  )    
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA01" ;    

COMMENT ON COLUMN Sys_DataLink.id IS 'ID';
COMMENT ON COLUMN Sys_DataLink.Result_type IS '1 = Single Result 2 = Multi Result (more than one row)';
COMMENT ON COLUMN Sys_DataLink.fr_table_name IS 'Link From Table Name';
COMMENT ON COLUMN Sys_DataLink.to_table_name IS 'Link To Table Name';
COMMENT ON COLUMN Sys_DataLink.join_clause IS 'Join criteria';
COMMENT ON COLUMN Sys_DataLink.descr IS 'Description';
COMMENT ON COLUMN Sys_DataLink.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Sys_DataLink.id_hopo IS 'Home Airport';
COMMENT ON COLUMN Sys_DataLink.data_source IS 'Data Source';
COMMENT ON COLUMN Sys_DataLink.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Sys_DataLink.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Sys_DataLink.created_user IS 'User or process which created this record';
COMMENT ON COLUMN Sys_DataLink.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN Sys_DataLink.opt_lock IS 'Optimistic Lock';


CREATE TABLE SYS_FUNCTION   (
ID VARCHAR(40) DEFAULT ' ' ,
NAME VARCHAR(30) DEFAULT ' ' ,
DESCR VARCHAR(100) DEFAULT ' ' ,
RET_TYPE NUMBER(1,0) DEFAULT 0 ,
PARAMS VARCHAR(2000) DEFAULT ' ' ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT 'YYZ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT SYSDATE ,
UPDATED_DATE DATE DEFAULT NULL ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0 
  )    
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA01" ;    


COMMENT ON COLUMN Sys_Function.id IS 'ID';
COMMENT ON COLUMN Sys_Function.name IS 'Name';
COMMENT ON COLUMN Sys_Function.descr IS 'Description';
COMMENT ON COLUMN Sys_Function.ret_type IS 'Return Data Type [0=Unknown, 1=Number, 2=Date, 3=String]';
COMMENT ON COLUMN Sys_Function.params IS 'Parameter Name and Their Types with Position Sequence';
COMMENT ON COLUMN Sys_Function.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Sys_Function.id_hopo IS 'Home Airport';
COMMENT ON COLUMN Sys_Function.data_source IS 'Data Source';
COMMENT ON COLUMN Sys_Function.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Sys_Function.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Sys_Function.created_user IS 'User or process which created this record';
COMMENT ON COLUMN Sys_Function.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN Sys_Function.opt_lock IS 'Optimistic Lock';



CREATE TABLE RULE_DETAIL (  
ID VARCHAR(40) DEFAULT ' ' ,
NAME VARCHAR(30) DEFAULT ' ' ,
DESCR VARCHAR(100) DEFAULT ' ' ,
RES_TYPE VARCHAR(3) DEFAULT 0 ,
RULE_TYPE NUMBER(2,0) DEFAULT 0 ,
DEF_SCORE NUMBER(10,0) DEFAULT 0 ,
MSG VARCHAR(4000) DEFAULT ' ' ,
CLIENT_RULE_CRITERIA1 VARCHAR(4000) DEFAULT ' ' ,
CLIENT_RULE_CRITERIA2 VARCHAR(4000) DEFAULT ' ' ,
SERVER_RULE_CRITERIA1 VARCHAR(4000) DEFAULT ' ' ,
SERVER_RULE_CRITERIA2 VARCHAR(4000) DEFAULT ' ' ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT 'YYZ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT SYSDATE ,
UPDATED_DATE DATE DEFAULT NULL ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0 
  )    
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA01" ;    


COMMENT ON COLUMN Rule_Detail.id IS 'ID';
COMMENT ON COLUMN Rule_Detail.name IS 'Name';
COMMENT ON COLUMN Rule_Detail.descr IS 'Description';
COMMENT ON COLUMN Rule_Detail.res_type IS 'Resource Type [e.g. PST=Parking Stand, GAT=GATE]';
COMMENT ON COLUMN Rule_Detail.rule_type IS 'Rule type [0=Unknown,-1=Hard rule,1=Soft rule]';
COMMENT ON COLUMN Rule_Detail.def_score IS 'Default Score (-ve for Hard rule, +ve for Soft rule)';
COMMENT ON COLUMN Rule_Detail.msg IS 'message when rule is broken';
COMMENT ON COLUMN Rule_Detail.client_rule_criteria1 IS 'Rule criteria for client use, part 1';
COMMENT ON COLUMN Rule_Detail.client_rule_criteria2 IS 'Rule criteria for client use, part 2';
COMMENT ON COLUMN Rule_Detail.server_rule_criteria1 IS 'Rule criteria for server use, part 1';
COMMENT ON COLUMN Rule_Detail.server_rule_criteria2 IS 'Rule criteria for server use, part 2';
COMMENT ON COLUMN Rule_Detail.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Rule_Detail.id_hopo IS 'Home Airport';
COMMENT ON COLUMN Rule_Detail.data_source IS 'Data Source';
COMMENT ON COLUMN Rule_Detail.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Rule_Detail.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Rule_Detail.created_user IS 'User or process which created this record';
COMMENT ON COLUMN Rule_Detail.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN Rule_Detail.opt_lock IS 'Optimistic Lock';


CREATE TABLE RULE_ASSIGNMENT (  
ID VARCHAR(40) DEFAULT ' ' ,
ID_RULE_DETAIL VARCHAR(40) DEFAULT ' ' ,
SCORE NUMBER(10,0) DEFAULT 0 ,
VALID_FROM DATE DEFAULT SYSDATE ,
VALID_TO DATE DEFAULT NULL ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT 'YYZ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT SYSDATE ,
UPDATED_DATE DATE DEFAULT NULL ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0 
  )    
  SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA01" ;    

COMMENT ON COLUMN Rule_Assignment.id IS 'ID';
COMMENT ON COLUMN Rule_Assignment.id_rule_detail IS 'ID of rule detail';
COMMENT ON COLUMN Rule_Assignment.score IS 'Score for this allocation';
COMMENT ON COLUMN Rule_Assignment.valiad_from IS 'Valid From Date Time';
COMMENT ON COLUMN Rule_Assignment.valiad_from IS 'Valid To Date Time';
COMMENT ON COLUMN Rule_Assignment.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN Rule_Assignment.id_hopo IS 'Home Airport';
COMMENT ON COLUMN Rule_Assignment.data_source IS 'Data Source';
COMMENT ON COLUMN Rule_Assignment.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN Rule_Assignment.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN Rule_Assignment.created_user IS 'User or process which created this record';
COMMENT ON COLUMN Rule_Assignment.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN Rule_Assignment.opt_lock IS 'Optimistic Lock';


CREATE TABLE RULE_ASSIGNMENT_DETAIL (
ID VARCHAR(40) DEFAULT ' ' ,
ID_RULE_ASSIGNMENT VARCHAR(40) DEFAULT ' ' ,
ALLOC_TYPE NUMBER(1,0) DEFAULT 0 ,
RES_ID VARCHAR(4000) DEFAULT ' ' ,
REC_STATUS CHAR(1) DEFAULT ' ' ,
ID_HOPO VARCHAR2(4) DEFAULT 'YYZ' ,
DATA_SOURCE VARCHAR2(8) DEFAULT NULL ,
CREATED_DATE DATE DEFAULT SYSDATE ,
UPDATED_DATE DATE DEFAULT NULL ,
CREATED_USER VARCHAR2(32) DEFAULT NULL ,
UPDATED_USER VARCHAR2(32) DEFAULT NULL ,
OPT_LOCK NUMBER(22,0) DEFAULT 0 
  ) SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE    
  (    
    INITIAL 20971520 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT    
  )    
  TABLESPACE "CEDA01" ;    


COMMENT ON COLUMN  Rule_Assignment_Detail.id IS 'ID';
COMMENT ON COLUMN  Rule_Assignment_Detail.id_rule_assignment IS 'ID of rule Rule_assignment';
COMMENT ON COLUMN  Rule_Assignment_Detail.alloc_type IS 'Allocation Type 0 = Individual (Take from  respective resource table- PST/GAT/�) 1 = Group (Use GGGTAB to get the resources under this group)';
COMMENT ON COLUMN  Rule_Assignment_Detail.Res_id IS 'Resource Type ID';
COMMENT ON COLUMN  Rule_Assignment_Detail.rec_status IS 'Record status X-delete I-invalid D-disable blank-valid';
COMMENT ON COLUMN  Rule_Assignment_Detail.id_hopo IS 'Home Airport';
COMMENT ON COLUMN  Rule_Assignment_Detail.data_source IS 'Data Source';
COMMENT ON COLUMN  Rule_Assignment_Detail.created_date IS 'Record creation Date Time - UTC';
COMMENT ON COLUMN  Rule_Assignment_Detail.updated_date IS 'Record last update Date Time - UTC';
COMMENT ON COLUMN  Rule_Assignment_Detail.created_user IS 'User or process which created this record';
COMMENT ON COLUMN  Rule_Assignment_Detail.updated_user IS 'User or process which modify this record';
COMMENT ON COLUMN  Rule_Assignment_Detail.opt_lock IS 'Optimistic Lock';



commit;