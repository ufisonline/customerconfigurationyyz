--------------------------------------------------------
--  File created - Thursday-May-07-2015   
--------------------------------------------------------
DROP TABLE "CEDA"."SYS_FUNCTION";
--------------------------------------------------------
--  DDL for Table SYS_FUNCTION
--------------------------------------------------------

  CREATE TABLE "CEDA"."SYS_FUNCTION" 
   (	"ID" VARCHAR2(40 BYTE) DEFAULT ' ', 
	"NAME" VARCHAR2(30 BYTE) DEFAULT ' ', 
	"DESCR" VARCHAR2(100 BYTE) DEFAULT ' ', 
	"RET_TYPE" NUMBER(1,0) DEFAULT 0, 
	"PARAMS" VARCHAR2(2000 BYTE) DEFAULT ' ', 
	"REC_STATUS" CHAR(1 BYTE) DEFAULT ' ', 
	"ID_HOPO" VARCHAR2(4 BYTE) DEFAULT 'HAJ', 
	"DATA_SOURCE" VARCHAR2(8 BYTE) DEFAULT NULL, 
	"CREATED_DATE" DATE DEFAULT SYSDATE, 
	"UPDATED_DATE" DATE DEFAULT NULL, 
	"CREATED_USER" VARCHAR2(32 BYTE) DEFAULT NULL, 
	"UPDATED_USER" VARCHAR2(32 BYTE) DEFAULT NULL, 
	"OPT_LOCK" NUMBER(22,0) DEFAULT 0
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 5242880 NEXT 10485760 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CEDA01" ;
 

   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."ID" IS 'ID';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."NAME" IS 'Name';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."DESCR" IS 'Description';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."RET_TYPE" IS 'Return Data Type [0=Unknown, 1=Number, 2=Date, 3=String]';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."PARAMS" IS 'Parameter Name and Their Types with Position Sequence';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."REC_STATUS" IS 'Record status X-delete I-invalid D-disable blank-valid';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."ID_HOPO" IS 'Home Airport';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."DATA_SOURCE" IS 'Data Source';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."CREATED_DATE" IS 'Record creation Date Time - UTC';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."UPDATED_DATE" IS 'Record last update Date Time - UTC';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."CREATED_USER" IS 'User or process which created this record';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."UPDATED_USER" IS 'User or process which modify this record';
 
   COMMENT ON COLUMN "CEDA"."SYS_FUNCTION"."OPT_LOCK" IS 'Optimistic Lock';
REM INSERTING into CEDA.SYS_FUNCTION
SET DEFINE OFF;
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('13','isWingDistanceAllow','Function to check clearance type for 2 parking stands',4,'[{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null"DataValue":null}{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null"DataValue":null}{"Name":"Name""Description":"Standard name for Clearance Type""DataType":30"Value":null"DataValue":"ICAO Standard;ICAO Standard|Airport Clearance 1;Airport Clearance 1"}]',' ',' ',null,to_date('03/02/15','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('9','fnNegate','Function to negate the input',4,'"[{"Name":"Condition""Description":"boolean value condition""DataType":4"Value":null}]]"',' ',' ',null,to_date('23/11/14','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('6','fnMaxFlightCount','Function to count total number of flights.',1,'[]',' ',' ',null,to_date('26/08/14','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('7','isSecondWithinFirst','Function to check if one date pair is within the range of the other',3,'[{"Name":"OuterTimeFrameStart""Description":"Start Date of Outer Time Frame""DataType":2"Value":null}{"Name":"OuterTimeFrameEnd""Description":"End Date of Outer Time Frame""DataType":2"Value":null}{"Name":"InnerTimeFrameStart""Description":"Start Date of Inner Time Frame""DataType":2"Value":null}{"Name":"InnerTimeFrameEnd""Description":"End Date of Inner Time Frame""DataType":2"Value":null}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('8','isEquals','Function to check if the given two strings are equal.',3,'[{"Name":"Source""Description":"Main String""DataType":3"Value":""}{"Name":"Target""Description":"Check if two strings are equal ""DataType":3"Value":null}]',' ',' ',null,to_date('04/09/14','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('1','getTwoDatesDiffInMinute','Function to calculate the difference between two date/time',1,'[{"Name":"DateTime1""Description":"Date/time 1""DataType":2"Value":null}{"Name":"DateTime2""Description":"Date/time 2""DataType":2"Value":null}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('2','addNumber','Function to add three numbers 
Example :
Number1 :1
Number2 :2
 $addnumber1 = 3 ',1,'[{"Name":"Number1""Description":"Number 1 Example :1""DataType":1"Value":null}{"Name":"Number2""Description":"Number 2 Example :2""DataType":1"Value":null}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('5','findGapInMinutes','Function to calculate the difference between two date/time',1,'[{"Name":"FromDate1""Description":"From Date 1""DataType":2"Value":null}{"Name":"ToDate1""Description":"To Date 1""DataType":2"Value":null}{"Name":"FromDate2""Description":"From Date 2""DataType":2"Value":null}{"Name":"ToDate2""Description":"To Date 2""DataType":2"Value":null}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('3','isOverlap','Function to check overlap between 2 date pairs',3,'[{"Name":"FromDate1""Description":"From Date 1""DataType":2"Value":null}{"Name":"ToDate1""Description":"To Date 1""DataType":2"Value":null}{"Name":"FromDate2""Description":"From Date 2""DataType":2"Value":null}{"Name":"ToDate2""Description":"To Date 2""DataType":2"Value":null}{"Name":"Duration""Description":"Duration""DataType":1"Value":0}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('4','isContain','Function to check if the given string2 contains in the given string 2',3,'[{"Name":"Source""Description":"Main String""DataType":3"Value":null}{"Name":"Target""Description":"Target String to be searched""DataType":3"Value":null}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('11','fnHasAllGatesSameSector','Function to check whether all the input gates have the same sector or not.',4,'[{"Name":"GateGroup""Description":"List Group of GAT from GGGTAB""DataType":31"Value":null"DataValue":"GATTAB""ColumnName":"GNAM"}]',' ',' ',null,to_date('29/01/15','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('10','fnMaxPaxCountByGate','Function to calculate total GATE Passengers',1,'[{"Name":"GateGroup""Description":"List Group of GAT from GGGTAB""DataType":31"Value":null"DataValue":"GATTAB""ColumnName":"GNAM"}{"Name":"Number""Description":"Integer value of the attribute name""DataType":1"Value":null"DataValue":"ResourceAlloc"}]',' ',' ',null,to_date('22/01/15','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('12','fnSectorTypeTest','Function to test user define drop down',4,'[{"Name":"SectorType""Description":"System defined SectorType""DataType":30"Value":null"DataValue":"I;InternationalFlight|D;DomesticFlight"}{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null"DataValue":null}]',' ',' ',null,to_date('03/02/15','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('16','isSameFlight','Function to check two demands have the same flight or not.',3,'[{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null}{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null}]',' ',null,null,null,null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('14','DiffSector','Function to check different sector for adjacent demand',4,'[{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null"DataValue":null}{"Name":"ResourceAllocObj""Description":"ResourceAllocObj Type""DataType":32"Value":null"DataValue":null}{"Name":"Sector Type""Description":"Sector Type""DataType":30"Value":null"DataValue":"I;International|D;Domestic|D/S;Domestic or Shengen"}]',' ',' ',null,to_date('06/03/15','DD/MM/RR'),null,null,null,0);
Insert into CEDA.SYS_FUNCTION (ID,NAME,DESCR,RET_TYPE,PARAMS,REC_STATUS,ID_HOPO,DATA_SOURCE,CREATED_DATE,UPDATED_DATE,CREATED_USER,UPDATED_USER,OPT_LOCK) values ('15','fnTotalBaggageCount','Function to calculate total BELT Bags',1,'[]',' ','HAJ',null,to_date('15/04/15','DD/MM/RR'),null,null,null,0);
