/**
 * 
 * @author rdevakumar
 *
 */
class NMSCSKFilePreparsing {

	/**
	 * Formats weekly csked data into one line
	 * @param content
	 * @return
	 */
	String formatWeeklyFile(content) {

		String cskFileContent = content
		//Pattern match to group Flight Record with the corresponding Sector records
		String cskedFltRcdpattern = "^1\\w{2}"
		String cskedSecRcdpattern = "^2\\w{2}"
		StringBuilder contentBuilder = new StringBuilder()
		def contentArray = cskFileContent.split(System.getProperty("line.separator"))
		for (int i = 0; i < contentArray.length; i++) {
			String flightRecord = ""
			// The first three characters define the record type and Airline Code e.g 1MI,2MI
			if (contentArray[i].substring(0,3).matches(cskedFltRcdpattern)) {
				flightRecord = contentArray[i]
				if(flightRecord.length()>56){
					flightRecord=flightRecord.substring(0, flightRecord.length()-(flightRecord.length()-56))
				}
			}
			int j = i + 1
			while (true) {
				if (j < contentArray.length && (contentArray[j].substring(0, 3)).matches(cskedSecRcdpattern) ) {
					flightRecord = flightRecord +" "+ contentArray[j]
				} else {
					break
				}
				++j
			}
			i = j - 1
			contentBuilder.append(flightRecord+ "\r\n")
		}

		contentBuilder.toString()
	}
	/**
	 * Formats nms seasonal file for SATS, Groups multiline records into one line
	 * @param content
	 * @return
	 */
	String formatSeasonalFile(content) {
		String nmsFileContent = content
		def contentArray = nmsFileContent.split(System.getProperty("line.separator"))
		StringBuilder contentBuilder = new StringBuilder();
		for (int i = 0; i < contentArray.length; i++) {
			String flightRecord = "";
			//The record length for the first line always 54
			if (contentArray[i].length() >= 54) {
				flightRecord = contentArray[i]
			}
			int j = i + 1;
			while (true) {
				if (j < contentArray.length && contentArray[j].length() < 54) {
					flightRecord = flightRecord + contentArray[j]
				} else {
					break
				}
				++j;
			}
			i = j - 1;
			contentBuilder.append(flightRecord.subSequence(0,flightRecord.length() - 1)+ "\r\n")
		}
		contentBuilder.toString()
	}
}
